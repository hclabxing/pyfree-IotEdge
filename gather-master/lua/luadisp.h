#ifndef _LUA_DISP_H_
#define _LUA_DISP_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : luadisp.h
  *File Mark       : 
  *Summary         : 调用lua脚本
  *1）实现返回协议报文解析为信息点-值得数据集 
  *2）实现将下控信息点-值得数据信息转换为信道所需的协议报文
  *3）从脚本读取总召报文的解析
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#ifdef __cplusplus
}
#endif

#include <string>
#include <vector>
#include "Mutex.h"

class luadisp{
public:
	luadisp(void);
	~luadisp(void);
private:
	lua_State*  m_pLua;
	PYMutex m_lua_Mutex;
public:
	bool open(const char * path);
	bool close();
	/**
	* 从脚本中获取总召查询指令集
    * @param _cmd {string&} 返回指令缓存,格式"cmd,cmd,..."
    * @param _errmsg {string&} 异常信息缓存
	* @param _type {int&} 任务编号
	* @return {bool } 
	*/
	bool getTotalCallCMD(std::string &_cmd, std::string &_errmsg, int &_type);
    /**
	* 调用脚本解析接收到的指令,获得信息点态势
    * @param _cmd {string} 返回报文指令
	* @param _recives {string&} 信息点态势,格式"id,val;id,val;..."
    * @param _errmsg {string&} 异常信息缓存
	* @param SpecificDesc {string} 下控指令,部分解析需要接口下控指令才能实现报文解码
	* @return {bool } 
	*/
	bool getReciveIDVAL(const char* _cmd, std::string &_recives, std::string &_errmsg
		, std::string SpecificDesc="null");
    /**
	 * 根据信息点态势调用lua脚本的编码函数(getResponse)生成响应指令,
    * 主要在作为从站端,转发端口在接收上层应用的总召查询指令进行回应
	* @param comment_ {string} 多点态势描述信息,格式"id,val;id,val;..."
    * @param _cmd {string&} 返回指令缓存
    * @param _checkType {int&} 返回指令处置标记
    * @param _errmsg {string&} 异常信息缓存
	* @param respType {unit} 响应类型,1)查询,2)控制
	* @param task_id {unit} 任务编号
	* @return {bool } 
	*/
    bool getResponse(std::string comment_, std::string &cmd_, int &checkType_, std::string &errmsg_
    	, unsigned int respType=1,unsigned int task_id = 0);
    /**
	* 根据信息点态势调用lua脚本的编码函数(getDownControl)生成控制指令
	* @param exetype {int} 执行类型{1,查询;2,设值}
	* @param ptype {int} 信息点类型{遥信/遥测等}
	* @param addr {int} 地址编号
	* @param val {float} 值
    * @param _cmd {string&} 返回指令缓存
    * @param _checkType {int&} 返回指令处置标记
    * @param _errmsg {string&} 异常信息缓存
	* @param task_id {unit} 任务编号
	* @return {bool } 
	*/
	bool getDownControl(int exetype, int ptype, int addr,int val
		, std::string &_cmd, int &_checkType, std::string &_errmsg
		, unsigned int task_id = 0);
};

#endif
