#ifndef _DTYPE_DEF_H_
#define _DTYPE_DEF_H_

/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : dtypedef.h
  *File Mark       : 
  *Summary         : 自定义数据类型
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

namespace pyfree
{
enum PType
{
	OnYX = 1,	//遥信
	OnYC = 2,   //遥测
	PTDef = 0
};

enum ExeType
{
	OnGet = 1,	//查询
	OnSet,		//设值
	OnTran,		//转发
	ExeTDef = 0
};

};
#endif // _DTYPE_DEF_H_