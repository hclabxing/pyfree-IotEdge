#ifndef _CONFCHNANEL_H_
#define _CONFCHNANEL_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_channel.h
  *File Mark       : 
  *Summary         : 采集通道业务数据
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>
#include <list>
#include <vector>
#include <map>

#include "dtypedef.h"

namespace pyfree
{
struct SerialPort
{
	int         id;				//串口接口编号
	std::string name;
	int         BaudRate;
	int         DataBit;
	std::string Parity;
	int         StopBit;		
	
	int         readCount;		//读取等待次数
	int         readSleep;		//读取等待时间,串口转发时也会采用来限制发送频率
	int			com_model;	    //通信模式[1,应答 默认; 2,双向发送,全态势推送; 3,双向发送,单点态势推送]	
};

struct NetPort
{
	int         id;				//网络接口编号
	int         type;			//帧处理类型
	int         ptype;			//报文解析方式
	std::string name;			//名称
	std::string ip;				//网络地址
	int         port;			//网络端口
	bool        readHeartTime; //读取心跳
};

struct ProtocolDef
{
	int         id;				//协议编号
	std::string luafile;        //加载脚本
	int         downType;		//
	int         upType;			//
	int         totalcallsleep;	//总召唤间隔
	int         oldState;		//脚本解码函数二参数传值选择
	int         ykSetVal;		//遥控是否直接设值,不需要查询返回设值
	int         retValNull;     //串口是否允许响应指令为空
};

struct PFuncArg
{
	std::string name;	        //参数名
	int         paddr;			//选择点寄存器地址,限制同一个采集接口的点
};

struct PInfo
{
	int             id;			//点编号
	PType           type;		//点类型
	int             addr;		//寄存器内偏移地址
	float           value;		//值
	float           defvalue;	//默认值
	std::string     ipStr;		//ip地址
	unsigned long   dev_addr;	//ip地址整型表述或采集信道设备地址
	unsigned int    updateTime; //更新时间
	unsigned int    sendTime;	//发送时间
	bool            logFlag;	//值发生变化时是否需要进行业务链记录
	int             upInterval;	//非变化发送间隔
};

struct PFuncInfo : public PInfo
{
	std::string             expr;			//表达式
	std::vector<PFuncArg>   args;           //参数集
};

struct GatherAtts
{
	int         id;				//采集口编号
	int         channeltype;	//采集口类型
	int         channelid;		//接口编号,与gtype一起确定接口
	int         protocolid;		//协议编号
	std::string name;	        //
    std::string desc;           //
	bool        hexflag;		//Hex格式标识
	int         controlSleep;	//下控间隔
	int         timePush;	    //定时推送
	bool        linkInitKey;	//重连初始化开关,默认为false,对于有查询指令的设备可设置为true
};

struct Gather
{
	GatherAtts				atts;		
	std::list<PInfo>		pinfos;		//采集点
	std::list<PFuncInfo>	pfuncinfos; //计算点
};

struct  Transmit
{
	int         id;				//
	std::string	name;	        //
    std::string desc;           //
	int         channeltype;	//转发端口类型[串口/网口]
	int         channelid;		//转发端口编号
	int         protocolid;		//协议编号	
};

//采集/转发信道相关配置集
struct ComManagerDef
{
	std::map<int,Transmit>		trans;		//转发端口定义
	std::map<int,Gather>		gathers;	//采集端口定义
	//
	std::map<int,SerialPort>	spdefs;		//串口定义
	std::map<int,NetPort>		netdefs;	//网口定义
	std::map<int,ProtocolDef>	protodefs;	//协议定义
};

};

#endif // CHNANEL_CONF_H