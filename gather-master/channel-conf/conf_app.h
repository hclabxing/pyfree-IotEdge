#ifndef _CONF_APP_H_
#define _CONF_APP_H_
/**********************************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_pmap.h
  *File Mark       : 
  *Summary         : 程序配置信息
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ***********************************************************************************/
#include <string>

namespace pyfree
{
struct GatherConf
{
	GatherConf()
		: diskSymbol('D')
		, freeSizeLimit(10000)
		, gLogDir("log")
		, gatherPath("gather.xml")
		, gmapPath("gmap.xml")
	{

	};
	//信息存储盘符
	char diskSymbol;
	//存储路径所在磁盘剩余空间(MB)
	int freeSizeLimit;//MB
	//日志路径
	std::string gLogDir;
	//xml_path
	std::string gatherPath; //采集配置
	std::string gmapPath;	//映射配置
};

};

#endif //_CONF_APP_H_
