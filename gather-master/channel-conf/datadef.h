#ifndef _DATA_DEF_H_
#define _DATA_DEF_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : datadef.h
  *File Mark       : 
  *Summary         : 业务结构化信息定义
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#ifdef __linux__
#include <string.h>
#endif
#include <time.h>
#include <stdio.h>
#include <string>
#include <list>

#include "dtypedef.h"
#include "pfunc.h"

struct NetArg
{
	std::string name;	//名称
	std::string desc;	//
	std::string ipStr;	//网络地址字符串
	int port;			//端口
	int type;			//数据读写类型
	int ptype;			//协议解析类型
	bool hexFlag;		//16进制编码标记,暂时不生效
	bool heartFlag;		//是否开启心跳标记
	int timePush;		//定时巡检上送间隔
	bool linkInitKey;	//重链时初始化点值开关,默认为false
};

struct TotalCall
{
	unsigned long time_totalcall;
	std::list<std::string> cmd_totalcall;
};

struct ChannelAssistInfo
{
	ChannelAssistInfo()
		: heartTime(static_cast<unsigned int>(time(NULL)))
		, newcmd("")
		, controlF(false)
		, taskID(0)
		, taskIDUseful(0)
		, m_heart_reconnect_log(false)
		, m_link_flag(false)
		, linkInitKey(false)
		, m_open_fail_log(false)
		, m_open_success_log(false)
		, m_error_ret_max_log(false)
	{

	};
	//
	unsigned int heartTime;
	std::string newcmd;
	bool controlF;
	unsigned long taskID;
	unsigned int taskIDUseful;	//任务编号记录时间
	bool m_heart_reconnect_log;	//心跳检测重链接日志标记,连续出现只记录一次
	bool m_link_flag;			//成功连接过的标记
	bool linkInitKey;			//重链时初始化点值开关,默认为false
	//日记减负,串口使用
	bool m_open_fail_log;		//打开失败操作日志写入标记,连续告警只写入一次
	bool m_open_success_log;	//打开成功操作日志写入标记,连续告警只写入一次
	bool m_error_ret_max_log;	//读取数据错误失败多次告警日志写入标记,连续告警只写入一次
};

struct ChannelCmd
{
	ChannelCmd() 
	: len(0)
	, controlF(false)
	, taskID(0)
	, wait(0)
	, acsiif(false)
	, flag(pyfree::ipToInt("127.0.0.1"))
	{
		memset(Buf, 0, 512);
	};
	ChannelCmd(unsigned char *buf
	    , int nlen
		, bool _controlF=false
		, unsigned long _taskID=0
		, int wait_=0
		, bool acsiif_=false
		, unsigned long flag_=pyfree::ipToInt("127.0.0.1"))
		: len(nlen)
		, controlF(_controlF)
		, taskID(_taskID)
		, wait(wait_)
		, acsiif(acsiif_)
		, flag(flag_)
	{
		memset(Buf, 0, 512);
		memcpy(Buf, buf, nlen);
	};
	ChannelCmd& operator=(const ChannelCmd &rval)
	{
		if (this != &rval) {
			memset(Buf, 0, 512);
			if (rval.len <= 512) {
				memcpy(Buf, rval.Buf, rval.len);
				len = rval.len;
			}
			else {
				memcpy(Buf, rval.Buf, 512);
				len = 512;
			}
			controlF 	= rval.controlF;
			taskID 		= rval.taskID;
			wait 		= rval.wait;
			acsiif 		= rval.acsiif;
			flag		= rval.flag;
		}
		return *this;
	};
	unsigned char Buf[512];	//指令内容
	int len;				//指令长度
	bool controlF;			//下控标识,主动下控或总召指令
	unsigned long taskID;	//任务编号
	//串口特定参数
	int wait;				//是否等待
	bool acsiif;			//是否acsii格式
	//
	unsigned long flag;		//ip标识,用于net级联采集
};

#define RDCSIZE 1024
struct RDClient
{
	RDClient()
		: len(0)
	{
		memset(Buf, 0, RDCSIZE);
	};
	RDClient(unsigned char *buf, int nlen)
	{
		memset(Buf, 0, RDCSIZE);
		memcpy(Buf, buf, nlen);
		len = nlen;
	};
	~RDClient()
	{
	};
	RDClient& operator=(const RDClient &rval)
	{
		if (this != &rval)
		{
			memset(Buf, 0, RDCSIZE);
			memcpy(Buf, rval.Buf, rval.len);
			len = rval.len;
		}
		return *this;
	};
	int add(unsigned char *buf, int nlen)
	{
		try {
			memset(Buf + len, 0, RDCSIZE - len);
			memcpy(Buf + len, buf, nlen);
			len += nlen;
		}
		catch (...)
		{
			printf("RDClient::add error \r\n");
		}
		return len;
	};
	unsigned char Buf[RDCSIZE];
	int len;
};

struct RDG
{
	RDG() : flag(""), len(0)
	{
		memset(Buf, 0, 512);
	};
	RDG(unsigned char *buf, int nlen, std::string _f = "")
		: flag(_f)
	{
		memset(Buf, 0, 512);
		memcpy(Buf, buf, nlen);
		len = nlen;
	};
	RDG& operator=(const RDG &rval)
	{
		if (this != &rval) {
			memset(Buf, 0, 512);
			if (rval.len < 512) {
				memcpy(Buf, rval.Buf, rval.len);
				len = rval.len;
			}
			else {
				memcpy(Buf, rval.Buf, 512);
				len = 512;
			}
			flag = rval.flag;
		}
		return *this;
	};
	std::string flag;
	unsigned char Buf[512];
	int len;
};

//转发信道转发到采集信道的业务信息
struct TransmitToGChannel
{
	TransmitToGChannel() 
		: exeType(pyfree::ExeTDef)
		, pID(0)
		, pType(pyfree::PTDef)
		, val(0.0)
		, taskID(0)
		, ipInt(0)
	{

	};
	TransmitToGChannel(pyfree::ExeType	_exeType
		, unsigned int _pID
		, pyfree::PType _pType
		, float _val
		, unsigned long _taskID=0
		, unsigned long long ipInt_=0)
		: exeType(_exeType)
		, pID(_pID)
		, pType(_pType)
		, val(_val)
		, taskID(_taskID)
		, ipInt(ipInt_)
	{
	};
	TransmitToGChannel& operator=(const TransmitToGChannel &rval)
	{
		if (this == &rval) {
			return *this;
		}
		pID = rval.pID;
		exeType = rval.exeType;
		pType = rval.pType;
		val = rval.val;
		taskID = rval.taskID;
		ipInt = rval.ipInt;
		return *this;
	};
	pyfree::ExeType	exeType;
	unsigned int pID;
	pyfree::PType	pType;
	//unsigned int evtTimeS;
	//unsigned int evtTimeMS;
	float val;
	unsigned long taskID;
	//IP标记,用于net级联
	unsigned long long ipInt;
};
//采集信道转发到转发信道的业务信息
struct ChannelToTransmit
{
	ChannelToTransmit() : pType(pyfree::PTDef), pID(0)
		/*, evtTimeS((static_cast<unsigned int>(time(NULL)))), evtTimeMS(0)*/
		, val(0.0), changeF(false), taskID(0)
	{
	};
	ChannelToTransmit(pyfree::PType _pType, unsigned int _pID
		//, unsigned int _s, unsigned int _ms
		, float _val,bool _changeF=false,unsigned long _taskID=0)
	{
		pType = _pType;
		pID = _pID;
		//evtTimeS = _s;
		//evtTimeMS = _ms;
		val = _val;
		changeF = _changeF;
		taskID = _taskID;
	};
	ChannelToTransmit& operator=(const ChannelToTransmit &rval)
	{
		if (this == &rval) {
			return *this;
		}
		pType = rval.pType;
		pID = rval.pID;
		val = rval.val;
		//evtTimeS = rval.evtTimeS;
		//evtTimeMS = rval.evtTimeMS;
		changeF = rval.changeF;
		taskID = rval.taskID;
		return *this;
	};
	////////
	pyfree::PType pType;
	unsigned int pID;
	//unsigned int evtTimeS;
	//unsigned int evtTimeMS;
	float val;
	bool changeF;
	unsigned long taskID;
};
//2020-05-28增加,转发串口剖面数据表示
struct LiveData
{
    pyfree::PType type;
    float value;
    unsigned int update_time;
};
/////////////////////////////////////////////////////////////////////////////////////
struct GWDS
{
	GWDS() : ipInt(0), exeType(pyfree::ExeTDef), pID(0), pType(pyfree::PTDef), val(0.0), controlF(false), taskID(0)
	{

	};
	GWDS(unsigned long long _ipInt, pyfree::ExeType	_exeType, unsigned int _pID, pyfree::PType _pType, float _val
		,bool _controlF=false,unsigned long _taskID=0)
		: ipInt(_ipInt), exeType(_exeType), pID(_pID), pType(_pType), val(_val)
		, controlF(_controlF), taskID(_taskID)
	{
	};
	GWDS& operator=(const GWDS &rval)
	{
		if (this == &rval) {
			return *this;
		}
		ipInt = rval.ipInt;
		pID = rval.pID;
		exeType = rval.exeType;
		pType = rval.pType;
		val = rval.val;
		controlF = rval.controlF;
		taskID = rval.taskID;
		return *this;
	};
	unsigned long long ipInt;
	pyfree::ExeType	exeType;
	unsigned int pID;
	pyfree::PType	pType;
	//unsigned int evtTimeS;
	//unsigned int evtTimeMS;
	float val;
	bool controlF;
	unsigned long taskID;
};

#endif
