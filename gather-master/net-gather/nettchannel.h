#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _NET_T_CHANNEL_H_
#define _NET_T_CHANNEL_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : nettchannel.h
  *File Mark       : 
  *Summary         : 
  *1)网口转发接口,最为客户端端连接调度服务或第三方平台
  *2)如果连接本系统的调度服务,可以采用默认的协议,可以设置ptype值2或3
  *3)如果连接第三方平台,可以设置ptype值1,标识采用lua实现报文编解码
  *4)连接第三方如果设置ptype值2或3,则需要第三方根据默认协议开发接口
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "tchannel.h"

class SocketClinet;

class NetTChannel : public TranChannel,public acl::thread
{
public:
    /* 构造函数
     * @param tid_ {int} 转发口编号
	 * @return { } 
	 */
    NetTChannel(int tid_);
    ~NetTChannel();

    void* run();
private:
    /* 本实例初始化
     * @param tid_ {int} 转发口编号
	 * @return {void } 
	 */
    void init(int tid_);
    /* 从转发端口读取数据,将数据进行分帧和帧解析
	 * @return {void } 
	 */
    void read();
    /* 编码和封帧,先转发端口写入数据
	 * @return {void } 
	 */
    void write();
    /**
	 * 获取转发给上层应用的业务协议报文
	 * @param buf {char*} 报文指针
	 * @param size {int} 缓存空间大小
	 * @return {int } 报文长度
	 */
	int getBuffer(unsigned char * _buf, int size);
	/**
	 * 获取转发给上层应用的心跳协议报文
	 * @param buf {char*} 报文指针
	 * @param size {int} 缓存空间大小
	 * @return {int } 报文长度
	 */
	int getHeartBeatBuffer(unsigned char * buf, int size);
	/**
	 * 对来自上层应用的报文进行解析
	 * @param buf {char*} 报文指针
	 * @param len {int} 报文长度
	 * @return {int } <0时异常
	 */
	int AddFrame(const unsigned char *buf, int len);
    /**
	 * 根据lua脚本文件进行属于编码,将单点状态按约定协议推送给上层应用
	 * @param buf {char*} 报文指针
	 * @param size {int} 缓存空间大小
	 * @return {int } 报文长度
	 */
	int getBufferDef(unsigned char * _buf, int size);
	/**
	 * 根据lua脚本文件进行属于编码,将多点状态按约定协议推送给上层应用
	 * @param buf {char*} 报文指针
	 * @param size {int} 缓存空间大小
	 * @return {int } 报文长度
	 */
	int getBufferDefList(unsigned char * buf, int size);
	/**
	 * 程序内部定义给来自上层应用的报文解析,主要是针对系统的调度服务的单点下控
	 * @param buf {char*} 报文指针
	 * @param len {int} 报文长度
	 * @return {int } <0时异常
	 */
	int AddFrameDef(const unsigned char *buf, int len);
private:
    bool running;
    pyfree::NetPort netargs;    //转发口配置信息
    SocketClinet* ptr_client;   //数据通信的具体实现
    //
    unsigned int heartBeatWrite;//心跳标记
    //数据读取辅助缓存
    RDClient rdc_data;
	ChannelCmd rddata;
};

#endif 
