#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_GATHER_WRITE_H_
#define _SOCKET_GATHER_WRITE_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_gather_write.h
  *File Mark       : 
  *Summary         : 该线程向指定客户端写入数据,将数据协议编码并序列化
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

class SocketPrivate_ACL;
class SocketGather;

class SocketGatherWrite : public acl::thread
{
public:
	SocketGatherWrite(int _netType, SocketPrivate_ACL* socket_acl_, SocketGather *gsd_);
	~SocketGatherWrite(void);

	void* run();
private:
	bool running;			//线程运行标记
	int netType;			//数据读写处理类型
	SocketPrivate_ACL *socket_acl;	//数据通信接口
	SocketGather *gsd_ptr;		//数据处理接口
};

#endif