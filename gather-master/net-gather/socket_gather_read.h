#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_GATHER_READ_H_
#define _SOCKET_GATHER_READ_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_gather_read.h
  *File Mark       : 
  *Summary         : 该线程从各个客户端读取数据,并将数据分帧和接卸
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "hashmap.h"
#include "datadef.h"
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

class SocketPrivate_ACL;
class SocketGather;

class SocketGahterRead : public acl::thread
{
public:
	SocketGahterRead(int _netType,SocketPrivate_ACL* socket_acl_, SocketGather *gsd_);
	~SocketGahterRead(void);

	void* run();
private:
	/**
	 * 从采集口读取数据,主要由socket_acl接口读取数据,存入缓存bufs中,然后进行数据分帧或报文解析处理
	 * @return {void } 
	 */
	void read();
	/**
	 * 检测是否有新的连接接入,主要由socket_acl接口完成标记,该函数将标记信息读取处理
	 * @return {void } 
	 */
	void check_link();
	/**
	 * 定期上送本采集口的信息点状态,具体工作由gsd_ptr接口完成
	 * @return {void } 
	 */
	void data_time_up();
private:
	bool running;					//线程运行标记
	unsigned int routeLinkTime;
	int netType;					//数据读写处理类型
	SocketPrivate_ACL *socket_acl;	//数据通信接口
	SocketGather *gsd_ptr;		//数据处理接口
	//下列为辅助业务处理缓存
	std::map<KeyObj_GClient, RDClient> bufs;
};

#endif