#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PRODUCER_AMTT_ALIYUN_H_
#define _PRODUCER_AMTT_ALIYUN_H_
/***********************************************************************
  *Copyright 2020-04-12, pyfree
  *
  *File Name       : ProducerMqttAliyun.h
  *File Mark       : 
  *Summary         : 阿里云发布线程
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include <map>

#include "datadef.h"
#include "queuedata_single.h"

class ProducerMqttAliyun : public acl::thread
{
public:
	ProducerMqttAliyun(std::map<int, SvcDesc> svcTriples_);
	virtual ~ProducerMqttAliyun();
	//
	void* run();
private:
	ProducerMqttAliyun(const ProducerMqttAliyun&);
	ProducerMqttAliyun& operator=(const ProducerMqttAliyun&) { return *this; };
  /**
	 * 巡检各个服务的运行态势
	 * @return {void}
	 */
	void checkSvcState();
private:
	bool running;
	std::map<int, SvcDesc> svcTriples;
	QueueDataSingle<WCacheAliyun> *send_queue; //待下发采集端的缓存数据
};

#endif 
