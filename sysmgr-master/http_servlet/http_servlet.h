#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/***********************************************************************
  *Copyright 2020-04-12, pyfree
  *
  *File Name       : http_servlet.h
  *File Mark       : 
  *Summary         : http_servlet接口,处理web业务请求
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#ifndef _HTTP_SERVLET_H_
#define _HTTP_SERVLET_H_

#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "datadef.h"

class http_servlet : public acl::HttpServlet
{
public:
	http_servlet(acl::socket_stream* stream, acl::session* session);
	~http_servlet();

protected:
  /**
	 * 异常处理
   * @param req {HttpServletRequest& } web请求
   * @param res {HttpServletResponse } web响应
	 * @return {bool} 是否成功
	 */
	virtual bool doError(acl::HttpServletRequest&,
		acl::HttpServletResponse& res);
    /**
	 * 未知类型处理
   * @param req {HttpServletRequest& } web请求
   * @param res {HttpServletResponse } web响应
	 * @return {bool} 是否成功
	 */
	virtual bool doUnknown(acl::HttpServletRequest&,
		acl::HttpServletResponse& res);
    /**
	 * GET 方法
   * @param req {HttpServletRequest& } web请求
   * @param res {HttpServletResponse } web响应
	 * @return {bool} 是否成功
	 */
	virtual bool doGet(acl::HttpServletRequest& req,
		acl::HttpServletResponse& res);
    /**
	 * POST 方法
   * @param req {HttpServletRequest& } web请求
   * @param res {HttpServletResponse } web响应
	 * @return {bool} 是否成功
	 */
	virtual bool doPost(acl::HttpServletRequest& req,
		acl::HttpServletResponse& res);
  /**
	 * 页面刷新
   * @param req {HttpServletRequest& } web请求
   * @param res {HttpServletResponse } web响应
	 * @return {bool} 是否成功
	 */
  virtual  bool doResponse(acl::HttpServletRequest& req
    , acl::HttpServletResponse& res);
  /**
	 * Post请求,Content-Type: application/octet-stream
   * @param req {HttpServletRequest& } web请求
   * @param res {HttpServletResponse } web响应
	 * @return {bool} 是否成功
	 */
  virtual bool doOctetStream(acl::HttpServletRequest&
    , acl::HttpServletResponse&);
  /**
	 * GET 或 POST ,处理web文件下载 
   * @param req {HttpServletRequest& } web请求
   * @param res {HttpServletResponse } web响应
	 * @return {bool} 是否成功
	 */
  virtual bool doParams(acl::HttpServletRequest& req
    , acl::HttpServletResponse& res);
  /**
	 *  POST,处理文件上传或下控
   * @param req {HttpServletRequest& } web请求
   * @param res {HttpServletResponse } web响应
	 * @return {bool} 是否成功
	 */
  virtual bool doForms(acl::HttpServletRequest& req
    , acl::HttpServletResponse& res);
    /**
	 * 下控真正实现函数,响应web服务控制请求
   * @param req {HttpServletRequest& } web请求
   * @param res {HttpServletResponse } web响应
	 * @return {bool} 是否成功
	 */
  virtual bool doControl(acl::HttpServletRequest& req
    , acl::HttpServletResponse& res);
  /**
	 * 文件上传真正实现函数,Content-Type: multipart/form-data;
   * @param req {HttpServletRequest& } web请求
   * @param res {HttpServletResponse } web响应
	 * @return {bool} 是否成功
	 */
  virtual bool doUpload(acl::HttpServletRequest& req
    , acl::HttpServletResponse& res);
private:
  /**
	 * 根据服务配置信息生成web展示服务信息的xml描述格式
   * @param xml_desc {acl::string& } 返回缓存
	 * @return {bool}
	 */
  bool get_SVCFile_XMLDesc(acl::string &xml_desc);
  /**
	 * 构建web显示的html
   * @param buf {acl::string& } 返回缓存
	 * @return {void}
	 */
  void create_html_desc(acl::string &buf);
private:
  //服务信息缓存
  std::map<int, SvcDesc> svcTriples;
};

#endif
