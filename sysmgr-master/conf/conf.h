#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/***********************************************************************
  *Copyright 2020-04-12, pyfree
  *
  *File Name       : conf.h
  *File Mark       : 
  *Summary         : 业务配置信息
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#ifndef _CONF_H_
#define _CONF_H_

#include <string>

namespace pyfree
{
struct AliyunTriples
{
	AliyunTriples()
		: product_key("")
		, product_secret("")
		, device_name("")
		, device_secret("")
	{};
	bool invalid()
	{
		return (product_key.empty()
			|| product_secret.empty()
			|| device_name.empty()
			|| device_secret.empty());
	};
	std::string		product_key;		//产品key
	std::string		product_secret;		//产品密钥
	std::string		device_name;		//设备名
	std::string		device_secret;		//设备密钥
};

struct OTAAliyun
{
	OTAAliyun()
		: version_file("version.ini")
		, update_file("oat.zip")
		, zip_path("C:\\software\\7Z\\7z.exe")
		, update_dir("")
	{};
	std::string version_file;		//固件版本描述信息
	std::string update_file;		//升级固件包
	std::string zip_path;			//解压工具路径,目前采用zip
	std::string update_dir;			//升级缓存目录
	
};

struct WebConf
{
	WebConf() : ip("0.0.0.0"),port(8888)
	{};
	std::string ip;
	int port;
};

struct SysAssistConf
{
	SysAssistConf()
	: diskSymbol('D')
    , freeSizeLimit(10000)
    , dayForLimit(7)
    , gLogDir("log")
	{

	};
	char diskSymbol;		//信息存储盘符
	int freeSizeLimit;		//存储路径所在磁盘剩余空间(MB)
	int dayForLimit;		//日志保留最低天数
	std::string gLogDir;	//日志输出目录
};

struct ServiceConf
{
	ServiceConf() 
		: aliyunfunc(false)
		, svcdefine("svc.xml")
		, webfunc(true)
	{

	};
	bool aliyunfunc;
	AliyunTriples gateway;
	std::string svcdefine;
	OTAAliyun otaInfo;
	bool webfunc;
	WebConf webconf;
	//系统辅助功能配置
    SysAssistConf saconf;   //
};

struct ServiceInfo
{
	ServiceInfo() 
		: svc_name("")
		, app_dir("")
		, app_name("")
		, conf_ext("")
		, aliyun_key("")
		, upLoopTime(10)
		, dll_lib(false)
	{};
	ServiceInfo(const ServiceInfo& rval)
	{
		svc_name = rval.svc_name;
		app_dir = rval.app_dir;
		app_name = rval.app_name;
		conf_ext = rval.conf_ext;
		aliyun_key = rval.aliyun_key;
		upLoopTime = rval.upLoopTime;
		dll_lib = rval.dll_lib;
	};
	ServiceInfo& operator=(const ServiceInfo &rval)
	{
		if (this != &rval) {
			svc_name = rval.svc_name;
			app_dir = rval.app_dir;
			app_name = rval.app_name;
			conf_ext = rval.conf_ext;
			aliyun_key = rval.aliyun_key;
			upLoopTime = rval.upLoopTime;
			dll_lib = rval.dll_lib;
		}
		return *this;
	};

	std::string svc_name;	//服务名
	std::string app_dir;	//程序目录
	std::string app_name;	//程序名
	std::string conf_ext;	//配置文件扩展名集,采用;间隔
	std::string aliyun_key;	//阿里云物联网平台对应的网关设备的功能点key
	unsigned int upLoopTime;//推送阿里云物联网平台的时间间隔(数值不变时定期上送)
	bool dll_lib;			//更新是否带动态库
};
};

#endif
