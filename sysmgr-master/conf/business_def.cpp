#include "business_def.h"

#include "Log.h"
#include "xml_io.h"

namespace GlobalVar {
	extern std::string m_app_conf_file;
};

BusinessDef* BusinessDef::instance = NULL;
BusinessDef* BusinessDef::getInstance()
{
	if (NULL == BusinessDef::instance)
	{
		BusinessDef::instance = new BusinessDef();
	}
	return BusinessDef::instance;
}

BusinessDef::BusinessDef()
{
	init();
};
void BusinessDef::Destroy()
{
	if (NULL != BusinessDef::instance)
	{
		delete BusinessDef::instance;
		BusinessDef::instance = NULL;
	}
}

BusinessDef::~BusinessDef()
{
	this->Destroy();
}

void BusinessDef::init()
{
	pyfree::readAppConf(appConf,GlobalVar::m_app_conf_file);
	pyfree::readSvcMaps(svcmaps, appConf.svcdefine);
	CLogger::createInstance()->Log(MsgInfo, "load %s finish!", appConf.svcdefine.c_str());
}

bool BusinessDef::getSvcInfo(std::map<int, SvcDesc> &svcTriples)
{
	for (std::map<int, pyfree::ServiceInfo>::iterator it = svcmaps.begin(); it != svcmaps.end(); ++it)
	{
		SvcDesc svc_;
		svc_.svc_name = it->second.svc_name;
		svc_.app_dir = it->second.app_dir;
		svc_.app_name = it->second.app_name;
		svc_.conf_ext = it->second.conf_ext;
		svc_.aliyun_key = it->second.aliyun_key;
		svc_.upLoopTime = it->second.upLoopTime;
		svc_.dll_lib = it->second.dll_lib;
		svc_.markT = static_cast<unsigned int>(time(NULL));
		svcTriples[it->first] = svc_;
	}
	return !svcTriples.empty();
}

std::map<int, pyfree::ServiceInfo> BusinessDef::get_svcmaps()
{
	return svcmaps;
};
