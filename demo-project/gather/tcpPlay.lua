﻿--应用脚本
--脚本语言:LUA (http://www.lua.org )
--项目应用:采集软件与测试播放器通信解析脚本
--脚本版本:1.0
-----------------------------------------------------------------------------------------------------------------------------------------
function getTotalCallCMD()
	local allcall = ""
	return allcall,0
end

function DEC_HEX(IN)
    local B,K,OUT,I,D=16,"0123456789ABCDEF","",0
    while IN>0 do
        I=I+1
        IN,D=math.floor(IN/B),math.mod(IN,B)+1
        OUT=string.sub(K,D,D)..OUT
    end
    return OUT
end

function num2hex(num)
    local hexstr = '0123456789abcdef'
    local s = ''
    while num > 0 do
        local mod = math.fmod(num, 16)
        s = string.sub(hexstr, mod+1, mod+1) .. s
        num = math.floor(num / 16)
    end
    if s == '' then s = '0' end
    return s
end

function getDownControl(exetype, ptype, addr, val)
	--collectgarbage("collect")
	local controlCmd = "NULL"
	local check = 2
	--print("..1..")
	if 1==exetype then
		--print("..1..")
		controlCmd = ""
	else
		--print("..2..")
		local intStr = string.format("%08X",addr)
		--print(intStr)
		local valStr = string.format("%08X",val)
		--print(valStr)
		local intStr_ = string.sub( intStr, 7, 8 )..string.sub( intStr, 5, 6 )..string.sub( intStr, 3, 4 )..string.sub( intStr, 1, 2 )
		local valStr_ = string.sub( valStr, 7, 8 )..string.sub( valStr, 5, 6 )..string.sub( valStr, 3, 4 )..string.sub( valStr, 1, 2 )
		if 1==ptype then
			controlCmd = "F3".."0C00"..intStr_..valStr_.."FF"
		elseif 2==ptype then
			controlCmd = "F4".."0C00"..intStr_..valStr_.."FF"
		else
			controlCmd = ""
		end
		--print(controlCmd)
	end
	--print("..3..")
	return controlCmd,check
end

function hex2string(hex)
	local str, n = hex:gsub("(%x%x)[ ]?", function (word)
	return string.char(tonumber(word, 16))
	end)
	return str
end

function getReciveIDVAL(up_cmd,down_cmd)
	local stra = up_cmd
	--print(stra)
	local recs = ""
	--print("...1...\n")
	local hstr = string.sub( stra, 1, 2 )
	local ptype = 0;
	if hstr=="F3" then
		ptype = 1
	elseif hstr=="F4" then
		ptype = 2
	else
		return recs,0
	end
	--print("...2...\n")
	if string.len(stra)<12 then
		return recs,0
	end
	--print("...3...\n")
	if 0~=(string.len(stra)%2) then
		return recs,0
	end
	--print("...4...\n")
	local lLen = string.sub( stra, 3, 4 )
	local hLen = string.sub( stra, 5, 6 )
	local dlen = tonumber("0x"..lLen)+16*tonumber("0x"..hLen)
	--print(dlen)
	if dlen<=0 then
		return recs,0
	end
	--print("...5...\n")
	local size = 0;
	local id04 = string.sub( stra, 7, 8 )
	local id03 = string.sub( stra, 9, 10 )
	local id02 = string.sub( stra, 11, 12 )
	local id01 = string.sub( stra, 13, 14 )
	--print(id04..id03..id02..id01)
	local idx = tonumber("0X"..id04)+256*tonumber("0X"..id03)+65536*tonumber("0X"..id02)+16777216*tonumber("0X"..id01)	
	--print(idx)
	local val04 = string.sub( stra, 15, 16 )
	local val03 = string.sub( stra, 17, 18 )
	local val02 = string.sub( stra, 19, 20 )
	local val01 = string.sub( stra, 21, 22 )
	--print(val04..val03..val02..val01)
	local val = tonumber("0X"..val04)+256*tonumber("0X"..val03)+65536*tonumber("0X"..val02)+16777216*tonumber("0X"..val01)

	local valmap = string.format("%d,%d",idx,val)
	--print(valmap)
	recs = recs..valmap
	size = 1

	--print("...6...\n")
	return recs,size
	--return "1,2;5,6",2
end
