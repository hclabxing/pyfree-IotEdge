﻿--TCP/IP通信界些脚本
--脚本语言:LUA (http://www.lua.org )
--项目应用:采集软件与调度软件通信
--脚本版本:1.0
----------------------------------------------------------------------------------------------------------------------------------------
function DEC_HEX(IN)
    local B,K,OUT,I,D=16,"0123456789ABCDEF","",0
    while IN>0 do
        I=I+1
        IN,D=math.floor(IN/B),math.mod(IN,B)+1
        OUT=string.sub(K,D,D)..OUT
    end
    return OUT
end

function num2hex(num)
    local hexstr = '0123456789abcdef'
    local s = ''
    while num > 0 do
        local mod = math.fmod(num, 16)
        s = string.sub(hexstr, mod+1, mod+1) .. s
        num = math.floor(num / 16)
    end
    if s == '' then s = '0' end
    return s
end

function hex2string(hex)
	local str, n = hex:gsub("(%x%x)[ ]?", function (word)
	return string.char(tonumber(word, 16))
	end)
	return str
end

function tobinary32(num)
	local tmp = num
	local str = ""
	repeat
	if tmp % 2 == 1 then
		str = str.."1"
	else
		str = str.."0"
	end
	tmp = math.modf(tmp/2)
	until(tmp == 0)
	str = string.reverse(str)
	local lens = string.len(str)
	local as = 32 - lens
	if as < 0 then

	elseif as > 0 then
		for i=1,as do
			str = "0"..str
		end
	end

	return str
end

function hexToFloat( hexString )
	local tmpNum = tobinary32(tonumber(hexString,16))

	local sign = string.sub(tmpNum,1,1)
	sign = tonumber(sign)
	local exponent = string.sub(tmpNum,2,9)
	exponent = tonumber(exponent,2) - 127
	local mantissa = string.sub(tmpNum,10)
	mantissa = tonumber(mantissa,2)
	for i=1,23 do
		mantissa = mantissa / 2
	end
	mantissa = 1+mantissa
	-- print(mantissa)
	local result = (-1)^sign * mantissa * 2^exponent
	return result
end

function FloatToHex( floatNum )
	local S = 0
	local E = 0
	local M = 0
	if floatNum == 0 then
        return "00000000"
    end
	local num1,num2 = math.modf(floatNum/1)
	local InterPart = num1
 
	if floatNum > 0 then
		S = 0 * 2^31
	else
		S = 1 * 2^31
	end
	local intercount = 0
	repeat
		num1 = math.modf(num1/2)
		intercount = intercount + 1
	until (num1 == 0)
 
	E = intercount - 1
	local Elen = 23 - E
	InterPart = InterPart % (2^E)
	InterPart = InterPart * (2^Elen)
 
	E = E + 127
	E = E * 2^23
 
	for i=1,Elen do
		num2 = num2 * 2
		num1,num2 = math.modf(num2/1)
		M = M + num1 * 2^(Elen - i)
	end
 
	M = InterPart + M
 
	--E值为整数部分转成二进制数后左移位数：22.8125 转成二进制10110.1101，左移4位 1.01101101
    --E=4 ，再加上127 就为所需E值
	--010000011 01101101 000000000000000
 
	local Result = S + E + M
 
	Result = string.format("%08X",Result)
	return Result
end

-----------------------------------------------------------------------------------------------------------------------------------------
function getTotalCallCMD()
	local allcall = ""
	return allcall,0
end

function getDownControl(exetype, ptype, addr, val, allYXState)
	--collectgarbage("collect")
	local controlCmd = "NULL"
	local check = 2
	--print(string.format("%d,%d,%d,%.3f",exetype,ptype,addr,val))
	if 2==exetype then
		local lenStr = string.format("%04X",16)
		--print(lenStr)
		local intStr = string.format("%08X",addr)
		--print(intStr)
		local ptypeStr = string.format("%02X",ptype)
		--print(ptypeStr)
		local valStr = FloatToHex(val)
		--print(valStr)
		local taskStr = string.format("%08X",allYXState)
		--print(taskStr)
		local lenStr_ = string.sub( lenStr, 3, 4 )..string.sub( lenStr, 1, 2 )
		local intStr_ = string.sub( intStr, 7, 8 )..string.sub( intStr, 5, 6 )..string.sub( intStr, 3, 4 )..string.sub( intStr, 1, 2 )
		local taskStr = string.sub( taskStr, 7, 8 )..string.sub( taskStr, 5, 6 )..string.sub( taskStr, 3, 4 )..string.sub( taskStr, 1, 2 )
		controlCmd = "F6"..lenStr_..intStr_..ptypeStr..valStr..taskStr.."FF"
		--print(controlCmd)
	else
		controlCmd = ""
	end
	--print(controlCmd)
	return controlCmd,check
end

function getReciveIDVAL(up_cmd,down_cmd)
	local stra = up_cmd
	--print(stra)
	local recs = ""
	if string.sub( stra, 1, 1 )~="F" then
		return recs,0
	end
	if string.sub( stra, 1, 2 )=="F9" then
		recs = "-1,1"
		return recs,1
	end
	if string.sub( stra, 1, 2 )~="F8" then
		return recs,0
	end
	local cmdsize = string.len(stra)
	if cmdsize<32 then
		return recs,0
	end
	--print(cmdsize)
	local lLen = string.sub( stra, 3, 4 )
	local hLen = string.sub( stra, 5, 6 )
	local dlen = tonumber("0x"..lLen)+16*tonumber("0x"..hLen)
	--print(dlen)
	if 2*dlen<32 then
		return recs,0
	end
	if cmdsize<2*dlen then
		return recs,0
	end
	local lsize = string.sub( stra, 7, 8 )
	local hsize = string.sub( stra, 9, 10 )
	local dsize = tonumber("0x"..lsize)+16*tonumber("0x"..hsize)
	--print(dsize)
	if dsize<1 then
		return recs,0
	end
	if cmdsize<(dsize*26+10) then
		return recs,0
	end
	--print(string.sub( stra, cmdsize-1, cmdsize ))
	if string.sub( stra, cmdsize-1, cmdsize )~="FF" then
		return recs,0
	end
	local size = 0
	local index = 10
	repeat
		local idx = tonumber("0X"..string.sub( stra, 1+index, 2+index ))
			+16*tonumber("0X"..string.sub( stra, 3+index, 4+index ))
			+256*tonumber("0X"..string.sub( stra, 5+index, 6+index ))
			+4096*tonumber("0X"..string.sub( stra, 7+index, 8+index ))
		--print(idx)	
		local ptypeStr = string.sub( stra, 9+index, 10+index )
		local pType = tonumber("0x"..ptypeStr)
		local val = hexToFloat(string.sub( stra, 11+index, 18+index ))
		--print(val)
		local task = tonumber("0X"..string.sub( stra, 19+index, 20+index ))
			+16*tonumber("0X"..string.sub( stra, 21+index, 22+index ))
			+256*tonumber("0X"..string.sub( stra, 23+index, 24+index ))
			+4096*tonumber("0X"..string.sub( stra, 25+index, 26+index ))

		recs = recs..string.format("%d,%.2f",idx,val)
		size = size + 1
		index = index + 26
		if index < 2*dlen then
			recs = recs..";"
		end
	until(index>=2*dlen)
	--print(recs)
	return recs,size
end
