#!/bin/bash
#@pyfree 2020-04-16,py8105@163.com
#创建程序目录
#run for root
#定义程序全局路径变量
#程序根目录
dir='/usr/local/sysmgr'

systemctl stop pyfreeMgr.service
systemctl disable pyfreeMgr.service
rm -f /usr/lib/systemd/system/pyfreeMgr.service

rm -rf $dir
