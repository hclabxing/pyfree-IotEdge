﻿--应用脚本
--脚本语言:LUA (http://www.lua.org )
--项目应用:pyfree项目
--脚本版本:1.0
-----------------------------------------------------------------------------------------------------------------------------------------
function getTotalCallCMD()
	local allcall = "01030000000A"
	return allcall,1
end

function getDownControl(exetype, ptype, addr, val)
	--collectgarbage("collect")
	local controlCmd = "NULL"
	local check = 1
	
	if 1==exetype then
		controlCmd = "01030000000AC5CD"
		check = 0
	else
		if 1==ptype then
			controlCmd = "0106"..string.format("%04X",addr)..string.format("%04X",val)
		elseif 2==ptype then
			controlCmd = "0106"..string.format("%04X",addr)..string.format("%04X",val)
		else
			return controlCmd,check
		end
	end

	return controlCmd,check
end

function getReciveIDVAL(up_cmd,down_cmd)
	--local c1 = collectgarbage("count");
	--print("start:",c1);
	local stra = up_cmd
	--print(stra)
	local recs = ""
	--print("...1...\n")
	if string.sub( stra, 1, 4 )=="0106" then
		local hid = string.sub( stra, 5, 6 )
		local lid = string.sub( stra, 7, 8 )
		local id = tonumber("0X"..hid)*256+tonumber("0X"..lid)
		
		local hval = string.sub( stra, 9, 10 )
		local lval = string.sub( stra, 11, 12 )
		local val = tonumber("0X"..hval)*256+tonumber("0X"..lval)
		
		recs = string.format("%d,%d",id,val)
		return recs,1
	end
	if string.sub( stra, 1, 4 )~="0103" then
		return recs,0
	end
	--print("...2...\n")
	if string.len(stra)<14 then
		return recs,0
	end
	--print("...3...\n")
	if 0~=(string.len(stra)%2) then
		return recs,0
	end
	--print("...4...\n")
	local lLen = string.sub( stra, 5, 6 )
	local dlen = tonumber("0x"..lLen)
	--print(dlen)
	if dlen<=0 then
		return recs,0
	end
	local size = 0
	--print("...5...\n")
	for i=1,2*dlen,4 do
		local hval = string.sub( stra, i+6, i+7 )
		local lval = string.sub( stra, i+8, i+9 )
		local val = tonumber("0X"..hval)*256+tonumber("0X"..lval)
		local valmap = string.format("%d,%d",size,val)
		--print(valmap)
		recs = recs..valmap
		size = size+1
		if i<2*dlen then
			recs = recs..";"
		end
	end
	--print("...6...\n")
	--local c2 = collectgarbage("count");
	--print("end:",c2);
	--print(recs)
	return recs,size
	--return "1,2;5,6",2
end
