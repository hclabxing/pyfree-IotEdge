#!/bin/bash
#@pyfree 2020-04-16,py8105@163.com
#创建程序目录
#run for root
#定义程序全局路径变量
#程序根目录
dir='/usr/local/monitor'

systemctl stop pyfreeMonitor.service
systemctl disable pyfreeMonitor.service
rm -f /usr/lib/systemd/system/pyfreeMonitor.service

rm -rf $dir

rm -f /usr/local/lib/libmosquitto.*
rm -f /usr/local/lib/libmosquittopp.*
rm -f /etc/ld.so.conf.d/mosquitto_lib.conf
#重新加载动态库，使其生效
ldconfig
