# pyfree-monitor-master

#### 介绍
用于实现设备供电、态势、IT资源等的监控调度，采用可定制的定时、定期、条件、启停、轮询等任务策略以及终端可视化监控实现设备自动化运维管理，与采集服务（pyfree-gather-master）配套使用

#### 软件说明  

##### 架构概述 
* 1)总概述:  
    调度软件通过配置任务策略实现自动化运维，通过告警策略配置实现实时监控，数据源来自各个采集软件的tcp/ip通信推送  
    ![组件及模块](../res/for_doc/dispatch.PNG)  

* 2)类图:   
    ![调度软件主要类](../res/for_doc/monitor_class.PNG)   
    
##### 功能概述  

* 1)实时数据:  
    实时数据采用区域-设备-点是三个层级展示，目前区域仅在调度软件推送上层应用才有意义，在每个调度软件内，只有一个区域信息  
    设备信息可以对应实际设备也可以是自由定义的虚拟设备，来自不同实际设备的采集点可以自由配置挂载到各个定义设备上  
    可以为设备创建虚拟点，目前虚拟点用于实现更复杂的组态任务调度、告警策略配置，后续也会加入虚拟点的基于实际点的加权计算表达  
    实时数据在数值变化或定期推送给上层应用或第三平台  
    实时数据的业务结构可在dev.xml进行配置  
  
* 2)数据记录:  
    通过udp通信将数据推送到记录软件实现历史数据存储。    
  
* 3)任务调度(详见后面的编外知识扩展)：  
    任务调度涉及的判定条件有数值变化、时间、程序启停等划分为不同任务策略，并不同条件可以进行组合，例如轮询策略任务中，加上如时间范围的限制  
    通过虚拟点进行串联，可以实现不同任务策略的组合策略，实现复杂逻辑的调度任务  
    任务调度线程实时检索各个配置任务的条件，满足条件时会构建控制信息进行立刻执行，也可以将其加入执行队列中由其他线程处理  
    具体任务策略及其配置在plan.xml实现,请参看demo-project/monitor/plan.xml。  
  
* 4)告警策略：  
    告警分析目前分为三个方面，  
    * 其一，在实时数据更新是进行判定，实现对信号扰动、变位、越限等的告警，这将要的设备的各个信息点下(dev.xml可配置)自行配置其判定依据  
    * 其二，在告警策略配置文件(alarm.xml)里配置，其实现类似于任务调度策略配置看，区别只是其执行指令是发布告警信息，而任务调度执行下控指令  
    * 其三，在任务调度、终端下控、上层应用设值等下控执行，需要在有限时间内检测预期与实际返回是否一致  
    目前告警信息发布分为日志输出、短信、邮件，当前告警信息的过滤策略相对简单，可能会造成信息冗余  
  
* 5)对外接口：  
    * 其一，实现了设备态势、事件推送阿里云物联平台的接口  
    * 其二，实现了设备态势、事件推送mqtt服务的接口  
    * 其三，实现了设备态势、事件推送第三平台的tcp-sokcet通信接口  
    * 其四，实现了告警信息推送阿里云短信服务的接口  
    * 其五，实现了邮件发布的接口(测试了163邮箱提供的邮件服务)  
    * 其六, 实现了从聚合天气服务获取天气信息的接口(阿里云市场提供)   

#### 编译
##### 一、依赖
1.  acl_master  
acl_master是一个跨平台c/c++库，提供了网络通信库及服务器编程框架，同时提供更多的实用功能库及示例，具体编译与实现请参考其说明文档。  
项目地址: https://gitee.com/acl-dev/acl 或 https://github.com/acl-dev/acl  
技术博客：https://www.iteye.com/blog/user/zsxxsz  

做了两处源码调整:  
由于acl提供的日志记录的时间只到秒级别，本项目需要毫秒的级别，因此修改了一下其源码，  
在lib_acl/src/stdlib/acl_mylog.c文件中，将acl_logtime_fmt函数实现调整为：  
```
void acl_logtime_fmt(char *buf, size_t size)  
{  
	//time_t	now;  
	struct timeval tm0;  
	gettimeofday(&tm0, NULL);  
	time_t	now = tm0.tv_sec;  
#ifdef	ACL_UNIX  
	struct tm local_time;  
  
	//(void) time (&now);  
	(void) localtime_r(&now, &local_time);  
	strftime(buf, size, "%Y/%m/%d %H:%M:%S", &local_time);  
	sprintf(buf, "%s.%03d ", buf,(int)(tm0.tv_usec/1000));  
#elif	defined(ACL_WINDOWS)  
	struct tm *local_time;  
  
	//(void) time (&now);  
	local_time = localtime(&now);  
	strftime(buf, size, "%Y/%m/%d %H:%M:%S", local_time);  
	sprintf(buf, "%s.%03d ",buf, (int)(tm0.tv_usec/1000));  
#else  
# error "unknown OS type"  
#endif  
}   
```
如果不需要毫秒级的时间格式，就不必修改。  

其二:  
需要修改acl库的源码,在lib_acl/src/stdlib/iostuff/acl_readable.c的int acl_readable(ACL_SOCKET fd)函数中
```
fds.events = POLLIN | POLLPRI;
```
由于win中调用了WSAPoll函数，而Winsock provider不支持POLLPRI与POLLWRBAND,调整为:
```
fds.events = POLLIN | POLLPRI;
#ifdef ACL_WINDOWS
fds.events &=~(POLLPRI|POLLWRBAND);
#endif
```

2.  sqlite  
sqlite为sqlite数据库的c接口，源码本项目经过整理为cmake工程，直接进入到sqlite目录按cmake编译即可  
  
3.  libuuid  
libuuid主要用于生成uuid使用，其下载地址：https://sourceforge.net/projects/libuuid/  
下载后解压,解压文件已自行下载的版本为准：tar -zxvf libuuid-1.0.3.tar.gz   
编译：  
cd libuuid-1.0.3   
./configure --prefix=you_dir/uuid \#不指定目录,默认/usr/local,注意不支持相对路径  
make && make install  

4.  iot_aliyun  
iot_aliyun目录是阿里云物联网平台提供的c++_SDK包编译的头文件和库，采用的是iotkit-embedded-2.3.0.zip源码编译的。  
最新版本可以去阿里云物联网平台官网下载，新版本有些函数会有变更，本调度软件涉及阿里云物联网接口部分需要作出调整。  
  
5.  mosquitto  
mosquitto是支持mqtt消息队列通信的c++库，其下载地址：http://mosquitto.org/download/  
具体编译请参考其说明文档，其config.mk文件有各种编译输出目录、依赖配置，请按说明配置  

##### 二、项目编译：

> linux编译
项目编译需要cmake+gcc支持，目前本项目的编译环境是centos7.3，由于需要将git相关信息写入程序,需要安装git  
或在CMakeLists.txt中注销:"execute_process(COMMAND /bin/bash ../build.sh)"  
编译命令类似如下:  
```
cd monitor-master 
mkdir build-linux 
cd build-linux 
cmake .. 
make
```

> win编译
当前win编译测试采用vs2015+cmake编译,例子如下  
备注:若需要32为支持请去掉Win64指定,msbuild为vs的命令编译工具,可以直接vs打开工程文件编译    
打开vs的命令行工具  
```
cd monitor-master  
mkdir build-win  
cd build-win  

cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Release ..  
msbuild pyfree-monitor.sln /p:Configuration="Release" /p:Platform="x64"
或
cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Debug ..  
msbuild pyfree-monitor.sln /p:Configuration="Debug" /p:Platform="x64"
```
由于acl第三方库win静态库有点大,本项目debug版本库不上传编译好的库,请自行编译,需要做一处调整
需要修改acl库的源码,在lib_acl/src/stdlib/iostuff/acl_readable.c的int acl_readable(ACL_SOCKET fd)函数中
```
fds.events = POLLIN | POLLPRI;
```
由于win中调用了WSAPoll函数，而Winsock provider不支持POLLPRI与POLLWRBAND,调整为:
```
fds.events = POLLIN | POLLPRI;
#ifdef ACL_WINDOWS
fds.events &=~(POLLPRI|POLLWRBAND);
#endif
```
#### demo示例

> linux  
1. 环境搭建  
主机win64系统  
在demo-project\client_test提供了简单的本地可视化控制终端(pyfree_client.exe),,与调度软件采用tcp-socket通信。  
如果需要linux或android版本的请去顶级目录下software_test\client\touch自行编译  
虚拟机  
采用VMware工具创建虚拟机,centos7.3系统,本样例的网络地址设置192.168.174.130,  
开启虚拟机的共享文件夹功能，将项目挂载到虚拟机上  
  
2.  程序配置  
编译的调度软件输出在demo-project/monitor  
程序启动有根据磁盘或网卡校验的License约束,与采集软件一致  
因此需要向生成License的sn.txt输出文件，启动虚拟机，进入其目录执行指令构建：  
./SWL 0 22 或 ./SWL 1 22  
生成License的sn.txt，如果与采集软件部署在同一台设备，生成一次就能拷贝过来使用  
如果没用SWL程序，去顶级目录下的swLicense项目编译生成,支持cmake编译  
  
3.  项目配置  
appconf.xml：主要设置程序需要的连接信息、对外接口信息配置以及设备配置文件、调度策略文件、告警策略文件等路径指定  
dev.xml：设备信息配置，具体看配置文件说明  
plan.xml：任务策略配置，具体看配置说明  
alarm.xml：告警策略配置，具体看配置说明  
pmap.xml：来自各个采集软件的点信息与各设备下点信息的映射配置，具体看配置说明  

> win64  
1.  程序配置  
在demo-project\client_test提供了简单的本地可视化控制终端(pyfree_client.exe),与调度软件采用tcp-socket通信  
编译的调度软件输出在demo-project/monitor  
程序启动有根据磁盘或网卡校验的License约束,与采集软件一致  
因此需要向生成License的sn.txt输出文件,进入其目录执行指令构建：  
SWL.exe 0 22 或 SWL,exe 1 22  
生成License的sn.txt，如果与采集软件部署在同一台设备，生成一次就能拷贝过来使用  
如果没用SWL程序，去顶级目录下的swLicense项目编译生成,支持cmake编译  
  
服务安装,管理员启动cmd,进入demo-project/monitor目录：  
安装:pyfree-monitor.exe install  
卸载:pyfree-monitor.exe uninstall  
在任务管理器或服务管理页面可以启停服务或配置其服务相关信息  
更新:在任务管理器停止服务,拷贝pyfree-monitor.exe覆盖完成更新  

2.  项目配置  
appconf.xml：主要设置程序需要的连接信息、对外接口信息配置以及设备配置文件、调度策略文件、告警策略文件等路径指定    
dev.xml：设备信息配置，具体看配置文件说明    
plan.xml：任务策略配置，具体看配置说明    
alarm.xml：告警策略配置，具体看配置说明    
pmap.xml：来自各个采集软件的点信息与各设备下点信息的映射配置，具体看配置说明    

> 阿里云物联网平台支持  
【1】需要注册阿里云账号，并开通物联网平台服务  
【2】在阿里云物联网平台创建网关产品    
![阿里云产品创建](../res/for_doc/aliyun_product.PNG) 
创建设备产品并定义设备产品功能  
![阿里云产品功能定义](../res/for_doc/aliyun_funckey.PNG) 
【3】创建网关设备以及实体设备的实例   
![阿里云设备创建](../res/for_doc/aliyun_devconf.PNG) 
并设备实例设置为网关设备的子设备  
![阿里云子设备绑定](../res/for_doc/aliyun_childdev.PNG)  
【4】在appconf.xml配置网关设备的三元组信息  
```
<AliyunConf>
    <!-- 阿里云物联网平台接口开关 -->
    <AliyunIoFunc>0</AliyunIoFunc>
    <!--网关设备三元组信息-->
    <product_key>a1D1cOroRQp</product_key>
    <product_secret>***</product_secret>
    <device_name>pyfree_zh</device_name>
    <device_secret>***</device_secret>
</AliyunConf>
```
【5】在dev.xml配置子设备的三元组信息以及对应产品下的点key信息  
```
	<dev devID="4" devType="3" GID="1" name="pcs" desc="播放器"
		product_key="a12BjHSYgZ6" product_secret="***" device_name="pyfree_zh01" device_secret="***">
		<pInfo pID="3" pType="1" name="onplaying" desc="播放状态" aliyun_key="play" UpLoopTime_ali="600"/>
		<pInfo pID="4" pType="1" name="onplay" desc="正播放状态" aliyun_key="playing" UpLoopTime_ali="600"/>
	</dev>
```

#### 编外知识扩展

* 物联网自动化(场景联动)策略:  
    系统的自动化或联动总是条件触发的，该触发点可能来着自与系统下端设备采集的设备态势或孪生数据，亦可能是上层应用的输入，又或是系统内在既定业务逻辑的驱动，这些自动化业务逻辑运转使得每个设备、场景、人等互相联动规则，这种规则模型称为 TCA 模型，一般由触发器（Trigger）、执行条件（Condition）、执行动作（Action）三个部分组成。  
    来自设备端的数据通常就数值性质分为遥信（开关）、遥测（电压），而依据其时间性质分为属性(尺寸)、态势(电流)。前者一般侧重数学逻辑，后者倾向于业务表述。很多时候我们也会将遥信、遥测统一表述为遥测,在开发实现上用float(double)等变量存储，遥信则看成特殊的float值(1.0,0.0)。同样的，属性、态势都可以归一化为态势，属性就是依时间推移不变的态势。  
    来自上层应用端的数据通常就数值性质分为遥控、遥调，其实与遥信、遥测相对应，在不少传统行业，四遥时会分开，但目前越来越多的应用会将遥信、遥控归并为遥信，遥测、遥调归并遥测，甚至将四遥归一。  
    现在解析TCA 模型在本系统的应用映射，对于本系统来说，来自设备端的数据采集、来自上层应用的数据下发以及系统内部的态势巡检线程均扮演本系统的触发器角色，而执行条件就是配置的任务策略、告警策略、清洗策略等构建的条件，详见plan.xml，alarm.xml,dev.xml配置条件表述。执行动作就是在这些条件满足后执行的业务逻辑，主要有三点，其实也是和三份构建条件一致的，任务策略的执行动作就是对某些信息点集进行设值、查询等，告警策略的执行动作就是按需生成告警信息并以指定的告警方式通知责任人，清洗策略的执行动作主要是确定数据推送、记录等，次要就是进行数据判定相关的告警，如数据扰动。  
    其中任务策略是本系统自动化的主要触发点，其执行条件的依据主要为三方面：  
    1）信息点数值匹配，信息点可能是来自是设备端直接采集到的，或是依据原始采集数据孪生的数据，或是与上层应用对接的信息点。例如，开关闭合为原生采集，联动灯管的开闭态=（开关1|开关2）为孪生数据，在本系统构建第三方系统的某设备的温度对应的信息点。其匹配表现为两点，其一就是数值比较(<,>,=),其二就是数据本身属性，是否发生变更、更新时间是否符合要求。   
    2）时间匹配,主要限定执行条件的时刻点或时间范围。  
    3）程序态势匹配,就是程序启动、运行、暂停、退出等态势构建执行条件的一部分。  
    另外需要注意的是执行条件的三个方面可以相互组合构建更服务的执行条件或连锁驱动。例如，在每天下午2点到4点期间，每间隔三分钟将点1设值为rand(100);在周一，系统退出需要短信告警；点1和点2数据变为0时，将点3设值为1，点3变为1时将某个设备关闭。如上种种均可以自由组配实现。 
    