#ifndef _CONF_READ_H_
#define _CONF_READ_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_read.h
  *File Mark       : 
  *Summary         : 业务数据读取函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <map>
#include <string>

#include "conf_app.h"
#include "conf_dev.h"
#include "conf_pmap.h"
#include "conf_plan.h"
#include "conf_alarm.h"

namespace pyfree
{
// bool isExist(std::string _xml = "appconf.xml");
/**
 * 从指定xml文件中读取程序的配置信息
 * @param conf {AppConf&} 程序配置信息
 * @param xml_ {string} xml文件名
 * @retuan {void} 
 */
void readAppConf(AppConf &conf, std::string xml_ = "appconf.xml");
/**
 * 向指定xml文件中写入程序的配置信息
 * @param conf {AppConf&} 程序配置信息
 * @param xml_ {string} xml文件名
 * @retuan {void} 
 */
void writeAppConf(const AppConf conf, std::string xml_ = "appconf.xml");
/**
 * 从指定xml文件中设备集信息
 * @param devs {map &} 设备信息集
 * @param xml_ {string} xml文件名
 * @retuan {void} 
 */
void readDevs(std::map<unsigned long long,Dev> &devs, std::string xml_ = "dev.xml");
/**
 * 从指定xml文件中读取来自各个采集信道的信息点到设备点的映射信息
 * @param pmaps {vector &} 映射信息
 * @param xml_ {string} xml文件名
 * @retuan {void} 
 */
void readPMaps(std::vector<PMap> &pmaps, std::string xml_ = "pmap.xml");
/**
 * 从指定xml文件中读取任务策略信息
 * @param plans {vector &} 任务策略信息
 * @param xml_ {string} xml文件名
 * @retuan {void} 
 */
void readPlans(std::vector<Plan> &plans, std::string xml_ = "plan.xml");
/**
 * 从指定xml文件中读取告警策略信息
 * @param alarms {vector &} 告警策略信息
 * @param xml_ {string} xml文件名
 * @retuan {void} 
 */
void readAlarms(std::vector<Alarm> &alarms, std::string xml_ = "alarm.xml");
};
#endif //CONF_READ_H