#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef HASH_MAP_H
#define HASH_MAP_H
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_read.h
  *File Mark       : 
  *Summary         : 自定义散列映射关系,用于业务数据Map映射和快读定位
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "dtypedef.h"

#include <map>
#include <string>

class KeyObj_FT
{
public:
	KeyObj_FT(std::string  _ipStr, int _id, pyfree::PType _type);
	//
	static long cmp_Key(const KeyObj_FT &obj1, const KeyObj_FT &obj2);

	std::string m_ipStr;
	int	m_id;
	pyfree::PType m_type;
	unsigned long long	m_ip;
private:
};
inline bool operator==(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) < 0; }
////////////////////////////////////////////////////////////////////////
template <class T>
class MyObj_FT
{
public:
	MyObj_FT();
	void insert(KeyObj_FT key, T val);
	bool getVal(KeyObj_FT key, T &_val);
	int size();
	//其他函数类似实现
private:
	std::map<KeyObj_FT, T> data;
};
////////////////////////////////////////////////
template <class T>
MyObj_FT<T>::MyObj_FT() {};

template <class T>
void MyObj_FT<T>::insert(KeyObj_FT key, T val)
{
	data[key] = val;
};

template <class T>
bool MyObj_FT<T>::getVal(KeyObj_FT key, T &_val)
{
	typename std::map<KeyObj_FT, T>::iterator it = data.find(key);
	if (it != data.end()) {
		_val = it->second;
		return true;
	}
	return false;
};
template <class T>
int MyObj_FT<T>::size()
{
	return static_cast<int>(data.size());
};

//////////////////////////////////////////////////////////
class KeyObj_TF
{
public:
	KeyObj_TF(long long _devID, int _pID);
	//
	static long long cmp_Key(const KeyObj_TF &obj1, const KeyObj_TF &obj2);

	long long	m_devID;
	int m_pID;
private:

};
inline bool operator==(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) < 0; }

template <class T>
class MyObj_TF
{
public:
	MyObj_TF();
	void insert(KeyObj_TF key, T val);
	bool getVal(KeyObj_TF key,T &_val);
	int size();
	//其他函数类似实现
private:
	std::map<KeyObj_TF, T> data;
};
////////////////////////////////////////////
template <class T>
MyObj_TF<T>::MyObj_TF() {};

template <class T>
void MyObj_TF<T>::insert(KeyObj_TF key, T val)
{
	data[key] = val;
};

template <class T>
bool MyObj_TF<T>::getVal(KeyObj_TF key,T &_val)
{
	typename std::map<KeyObj_TF, T>::iterator it = data.find(key);
	if (it != data.end()) {
		_val = it->second;
		return true;
	}
	return false;
};
template <class T>
int MyObj_TF<T>::size()
{
	return static_cast<int>(data.size());
};

//////////////////////////////////////
class KeyObj_Client
{
public:
	KeyObj_Client(std::string  _ipStr, int _port);
	//
	static long cmp_Key(const KeyObj_Client &obj1, const KeyObj_Client &obj2);

	std::string m_ipStr;
	int	m_port;
	unsigned long long	m_ip;
private:
};
inline bool operator==(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) < 0; }

#endif //HASH_MAP_H
