#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _DATA_DEF_H_
#define _DATA_DEF_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : datadef.h
  *File Mark       : 
  *Summary         : 业务数据定义集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
/*
业务逻辑数据模型
*/
#include <time.h>
#include <vector>
#include <map>
#include <string>
#ifdef __linux__
#include <string.h>
#include <stdio.h>
#endif
#include "dtypedef.h"

using namespace pyfree;

struct DataFromChannel
{
	DataFromChannel() : flag(""), len(0)
	{
		memset(Buf, 0, 512);
	};
	DataFromChannel(unsigned char *buf, int nlen, std::string _f = "")
		: flag(_f)
	{
		memset(Buf, 0, 512);
		memcpy(Buf, buf, nlen);
		len = nlen;
	};
	DataFromChannel& operator=(const DataFromChannel &rval)
	{
		if (this != &rval) {
			memset(Buf, 0, 512);
			if (rval.len < 512) {
				memcpy(Buf, rval.Buf, rval.len);
				len = rval.len;
			}
			else {
				memcpy(Buf, rval.Buf, 512);
				len = 512;
			}
			flag = rval.flag;
		}
		return *this;
	};
	std::string		flag;
	unsigned char	Buf[512];
	int len;
};


struct DataToGather
{
	DataToGather() : ipInt(0), exeType(ExeTDef)
		, pID(0), pType(PTDef), val(0.0), sleep(0), desc(""), taskID(0), virtualP(false)
	{

	};
	DataToGather(unsigned long long _ipInt, ExeType	_exeType
		, unsigned int _pID, PType _pType, float _val)
		: ipInt(_ipInt), exeType(_exeType)
		, pID(_pID), pType(_pType), val(_val)
		, sleep(0), desc(""), taskID(0), virtualP(false)
	{
	};
	DataToGather(unsigned long long _ipInt, ExeType	_exeType, unsigned int _pID, PType _pType, float _val
		, unsigned long _sleep,std::string _desc,unsigned long _taskID=0)
		: ipInt(_ipInt), exeType(_exeType)
		, pID(_pID), pType(_pType), val(_val)
		, sleep(_sleep), desc(_desc), taskID(_taskID), virtualP(false)
	{
	};

	DataToGather& operator=(const DataToGather &rval)
	{
		if (this == &rval) {
			return *this;
		}
		ipInt = rval.ipInt;
		pID = rval.pID;
		exeType = rval.exeType;
		pType = rval.pType;
		val = rval.val;
		sleep = rval.sleep;
		desc = rval.desc;
		taskID = rval.taskID;
		return *this;
	};
	unsigned long long ipInt;
	ExeType	exeType;
	unsigned int pID;
	PType	pType;
	//unsigned int evtTimeS;
	//unsigned int evtTimeMS;
	float val;
	unsigned long sleep;
	std::string desc;
	unsigned long taskID;
	bool virtualP;
};

struct DataToThird
{
	DataToThird() 
		: devID(0)
		, pID(0)
		, ptype(PTDef)
		, evtTimeS(static_cast<unsigned int>(time(NULL)))
		, evtTimeMS(0)
		, val(0.0)
		, len(0)
		, taskID(0)
	{
		memset(Buf, 0, 512);
	};
	DataToThird(unsigned long long _devID, unsigned int _pID, PType _ptype
		, unsigned int _s, unsigned int _ms, float _val
		, unsigned long _taskID = 0)
	{
		devID = _devID;
		pID = _pID;
		ptype = _ptype;
		evtTimeS = _s;
		evtTimeMS = _ms;
		val = _val;
		taskID = _taskID;
		len = 0;
		memset(Buf, 0, 512);
	};
	DataToThird(unsigned long long _devID, unsigned int _pID, PType _ptype
		, unsigned int _s, unsigned int _ms, float _val
		, unsigned char *buf, int nlen,unsigned long _taskID=0)
	{
		devID = _devID;
		pID = _pID;
		ptype = _ptype;
		evtTimeS = _s;
		evtTimeMS = _ms;
		val = _val;
		memset(Buf, 0, 512);
		memcpy(Buf, buf, nlen);
		len = nlen;
		taskID = _taskID;
	};
	DataToThird(const DataToThird &rval)
	{
		devID = rval.devID;
		pID = rval.pID;
		ptype = rval.ptype;
		val = rval.val;
		evtTimeS = rval.evtTimeS;
		evtTimeMS = rval.evtTimeMS;
		taskID = rval.taskID;

		memset(Buf, 0, 512);
		if (rval.len <= 512) {
			memcpy(Buf, rval.Buf, rval.len);
			len = rval.len;
		}
		else {
			memcpy(Buf, rval.Buf, 512);
			len = 512;
		}
	};
	DataToThird& operator=(const DataToThird &rval)
	{
		if (this == &rval) {
			return *this;
		}
		devID = rval.devID;
		pID = rval.pID;
		ptype = rval.ptype;
		val = rval.val;
		evtTimeS = rval.evtTimeS;
		evtTimeMS = rval.evtTimeMS;
		taskID = rval.taskID;

		memset(Buf, 0, 512);
		if (rval.len <= 512) {
			memcpy(Buf, rval.Buf, rval.len);
			len = rval.len;
		}
		else {
			memcpy(Buf, rval.Buf, 512);
			len = 512;
		}

		return *this;
	};
	////////
	unsigned long long devID;
	unsigned int	pID;
    PType           ptype;
	unsigned int	evtTimeS;
	unsigned int	evtTimeMS;
	float			val;
	unsigned char Buf[512];
	unsigned int len;
	unsigned long taskID;
};

struct SocketLocalWriteItem : DataToThird
{
	SocketLocalWriteItem(DataToThird item) 
		: DataToThird(item)
	{

	};
	SocketLocalWriteItem()
		: DataToThird()
	{

	};
};

struct SocketAliyunWriteItem : DataToThird
{
	SocketAliyunWriteItem(DataToThird item) 
		: DataToThird(item)
	{

	};
	SocketAliyunWriteItem()
		: DataToThird()
	{

	};
};

struct SocketMqttWriteItem : DataToThird
{
	SocketMqttWriteItem(DataToThird item) 
		: DataToThird(item)
	{

	};
	SocketMqttWriteItem()
		: DataToThird()
	{

	};
};

struct PValueRet
{
	PValueRet(float val_) 
		: val_actual(val_)
		, val_limitUp(0.0)
		, val_limitDown(0.0)
		, val_old(0.0)
		, val_old_clock(0)
		, shake_interval(0)
		, noChangeTimeLimit(0)
		, val_base(0.0)
		, val_rate(0.0)
		, sendflag(false)
		, sendflag_ali(false)
		, recordflag(false)
		, waringflag(false)
		, devDesc("")
		, pDesc("")
		, eway_(pyfree::AlarmForDef)
	{

	};
	float val_actual;
	float val_limitUp;
	float val_limitDown;
	float val_old;
	unsigned long long val_old_clock;
	unsigned int shake_interval;
	int  noChangeTimeLimit;
	float val_base;
	float val_rate;
	bool sendflag;
	bool sendflag_ali;
	bool recordflag;
	bool waringflag;
	std::vector<pyfree::EventType> evts;
	std::string devDesc;
	std::string pDesc;
	pyfree::EventWay	eway_;
};

#define RDCSIZE 1024
struct RDClient
{
	RDClient()
		: len(0)
	{
		memset(Buf, 0, RDCSIZE);
	};
	RDClient(unsigned char *buf, int nlen)
	{
		memset(Buf, 0, RDCSIZE);
		memcpy(Buf, buf, nlen);
		len = nlen;
	};
	~RDClient()
	{
	};
	RDClient& operator=(const RDClient &rval)
	{
		if (this != &rval)
		{
			memset(Buf, 0, RDCSIZE);
			memcpy(Buf, rval.Buf, rval.len);
			len = rval.len;
		}
		return *this;
	};
	int add(unsigned char *buf, int nlen)
	{
		try {
			memset(Buf + len, 0, RDCSIZE - len);
			memcpy(Buf + len, buf, nlen);
			len += nlen;
		}
		catch (...)
		{
			printf("RDClient::add error \r\n");
		}
		return len;
	};
	unsigned char Buf[RDCSIZE];
	int len;
};

enum TopicType
{
	TOPIC_DEV_LOGIN			= 1,		//子设备上线 Topic
	TOPIC_DEV_LOGIN_REPLY	= 2,		//子设备上线 Topic_Reply
	TOPIC_DEV_LOGOUT		= 3,		//子设备下线 Topic
	TOPIC_DEV_LOGOUT_REPLY	= 4,		//子设备下线 Topic_Reply
	//
	TOPIC_EVENT_PRO_POST		= 11,	//客户端上送数据 Topic
	TOPIC_EVENT_PRO_POST_REPLY	= 12,	//客户端上送数据 Topic_Reply
	//
	TOPIC_SERVICE_PRO_SET		= 21,	//服务设置属性Topic
	TOPIC_SERVICE_PRO_SET_REPLY = 22,	//
	//
	TOPIC_DEV_TAG_UPDATE		= 31,	//设备标签更新Topic
	TOPIC_DEV_TAG_UPDATE_REPLY	= 32,	//设备标签更新反馈Topic
	TOPIC_DEV_TAG_DEL			= 33,	//设备标签删除Topic
	TOPIC_DEV_TAG_DEL_REPLY		= 34,	//设备标签删除反馈Topic
	//
	TOPIC_DEV_DESIRED_GET		= 41,	//设备期望属性值获取Topic
	TOPIC_DEV_DESIRED_GET_REPLY = 42,	//设备期望属性值获取反馈Topic
	TOPIC_DEV_DESIRED_DEL		= 43,	//设备期望属性值删除Topic
	TOPIC_DEV_DESIRED_DEL_REPLY = 44,	//设备期望属性值删除反馈Topic
	//
	TOPIC_DEV_DISABLE			= 51,	//设备禁用Topic
	TOPIC_DEV_DISABLE_REPLY		= 52,	//设备禁用反馈Topic
	TOPIC_DEV_ENABLE			= 53,	//设备恢复Topic
	TOPIC_DEV_ENABLE_REPLY		= 54,	//设备恢复反馈Topic
	//
	//
	TOPIC_EVENT_CLIENT_UP		= 61,	//设备上报事件Topic
	TOPIC_EVENT_CLIENT_UP_REPLY = 62,	//设备上报事件反馈Topic
	//
	TOPIC_EVENT_SERVICE_DOWN	= 63,	//服务下行事件Topic
	TOPIC_EVENT_SERVICE_DOWN_REPLY = 64,//服务下行事件反馈Topic
	//
	TOPIC_DEFAULT = 0
};

struct AliyunDeviceMaps
{
	unsigned long long id;
	TopicType topicType;
	bool subcribe_flag;
	std::map<std::string, unsigned int> aliyun_keys;
};

struct RCacheAliyun
{
	RCacheAliyun() : topic(""), payload("")
	{};
	RCacheAliyun(std::string topic_, std::string payload_)
		: topic(topic_)
		, payload(payload_)
	{};
	RCacheAliyun& operator=(const RCacheAliyun &rval)
	{
		if (this != &rval) {
			topic = rval.topic;
			payload = rval.payload;
		}
		return *this;
	};
	std::string topic;
	std::string payload;
};

struct JsonEvent
{
	JsonEvent()
		: etime("")
		, etask("")
		, etype("")
		, elevel("")
		, earea("")
		, edev("")
		, epid("")
		, epval("")
	{
	};
	JsonEvent(std::string etime_
		, std::string etask_
		, std::string etype_
		, std::string elevel_
		, std::string earea_
		, std::string edev_
		, std::string epid_
		, std::string epval_)
		: etime(etime_)
		, etask(etask_)
		, etype(etype_)
		, elevel(elevel_)
		, earea(earea_)
		, edev(edev_)
		, epid(epid_)
		, epval(epval_)
	{
	};
	JsonEvent& operator = (const JsonEvent& rhs) //¸³Öµ·ûÖØÔØ  
	{
		if (this == &rhs)
		{
			return *this;
		}
		etime	= rhs.etime;
		etask	= rhs.etask;
		etype	= rhs.etype;
		elevel	= rhs.elevel;
		earea	= rhs.earea;
		edev	= rhs.edev;
		epid	= rhs.epid;
		epval	= rhs.epval;
		return *this;
	};
	std::string etime;
	std::string etask;
	std::string etype;
	std::string elevel;
	std::string earea;
	std::string edev;
	std::string epid;
	std::string epval;
};

struct JsonPValue
{
	JsonPValue() 
		: pID(0)
		, ptype(PTDef)
		, sec(0)
		, msec(0)
		, val(0.0)
	{
	};
	JsonPValue& operator = (const JsonPValue& rhs) //赋值符重载  
	{
		if (this == &rhs)
		{
			return *this;
		}
		pID = rhs.pID;
		ptype = rhs.ptype;
		sec = rhs.sec;
		msec = rhs.msec;
		val = rhs.val;
		return *this;
	};
	unsigned int pID;
	PType ptype;
	unsigned int sec;
	unsigned int msec;
	float val;
	//unsigned long taskID;
};

struct RecordItem
{
	int devid;
	int pid;
	unsigned int sec;
	unsigned int usec;
	float val;
};

#endif


