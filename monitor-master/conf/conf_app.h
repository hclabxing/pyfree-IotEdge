#ifndef _CONF_APP_H_
#define _CONF_APP_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_app.h
  *File Mark       : 
  *Summary         : 程序配置信息
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>
#include <map>
#include <vector>

namespace pyfree
{
struct AliyunTriples
{
	AliyunTriples() 
		: product_key("")
		, product_secret("")
		, device_name("")
		, device_secret("")
	{};
	bool invalid()
	{
		return (product_key.empty() 
			|| product_secret .empty()
			|| device_name.empty() 
			|| device_secret.empty());
	};
	std::string		product_key;		//产品key
	std::string		product_secret;		//产品密钥
	std::string		device_name;		//设备名
	std::string		device_secret;		//设备密钥
};
//阿里云物联网上送事件描述
struct ALiyunEventInfo
{
	//阿里云云物联网平台设备所对应的事件描述,当前只有一个事件，事件只有一个输出参数
	ALiyunEventInfo()
		: aliEventFlag("pyfree_event")
		, aliEventOut("etime;etask;etype;elevel;earea;edev;epid;epval")
	{};
	ALiyunEventInfo& operator=(const ALiyunEventInfo &rval)
	{
		if (this != &rval) {
			aliEventFlag = rval.aliEventFlag;
			aliEventOut = rval.aliEventOut;
		}
		return *this;
	};
	bool isValid()
	{
		return (!aliEventFlag.empty() && !aliEventOut.empty());
	};
	std::string aliEventFlag;
	std::string aliEventOut;
};
//
struct AliyunDevices
{
	AliyunDevices() : atriples(AliyunTriples())
	{};
	AliyunTriples atriples;
	std::map<unsigned int, std::string> aliyun_keys;//设备点的key,用于阿里云物联网
	std::map<std::string, std::string> dev_tags;	//设备标签集,用于阿里云物联网
};

struct AliyunConf
{
	AliyunConf(): aliyunIoFunc(false)
	{

	};
    bool aliyunIoFunc;			//阿里云物联网接口开关
	AliyunTriples   gateway;	//网关三元组
	ALiyunEventInfo aliEI;		//网关事件参数
};

struct SysAssistConf
{
	SysAssistConf()
	: diskSymbol('D')
    , freeSizeLimit(10000)
    , dayForLimit(7)
    , gLogDir("log")
	{

	};
	char diskSymbol;		//信息存储盘符
	int freeSizeLimit;		//存储路径所在磁盘剩余空间(MB)
	int dayForLimit;		//日志保留最低天数
	std::string gLogDir;	//日志输出目录
};

struct RecordConf
{
	RecordConf()
	: recordFunc(false)
	, peer_ip("127.0.0.1")
	, peer_port(60004)
	, local_ip("127.0.0.1")
	, local_port(70004)
	{

	};
	bool recordFunc;		//数据记录功能开关
	std::string peer_ip;	//udp地址
	int	peer_port;			//udp端口
	std::string local_ip;	//udp地址
	int	local_port;			//udp端口
};

struct WaringPushConf
{
	WaringPushConf()
	: wpFunc(false)
	, peer_ip("127.0.0.1")
	, peer_port(60005)
	, local_ip("127.0.0.1")
	, local_port(70005)
	{

	};
	bool wpFunc;			//告警推送功能开关
	std::string peer_ip;	//udp地址
	int	peer_port;			//udp端口
	std::string local_ip;	//udp地址
	int	local_port;			//udp端口
};

struct AreaInfo
{
	AreaInfo()
	: areaId(1)
    , areaType(1)
    , areaName("zhsye")
    , areaDesc("珠海金鼎")
	{

	};
	int areaId;					//区域编号
	int areaType;				//区域类型,暂无使用
	std::string areaName;		//区域类型,暂无使用
	std::string areaDesc;		//区域描述
};

struct HttpWeather 
{
	HttpWeather()
	: httpWeatherFunc(false)
    , startHourWeather(8)
    , endHourWeather(17)
    , threeHourCityWeather(0)
    , weatherhost("weather-ali.juheapi.com")
    , weatherpath("/weather/index")
    , weatherport(80)
    , weathercity("珠海")
    , weatherauthorization("APPCODE e64e5d45869e4df588326420859da9dc")
    , weatherUpTime(60)
	{

	};
    bool httpWeatherFunc;		//天气预报接口功能开关
	int startHourWeather;		//开始时间（天气-时辰）
	int endHourWeather;			//结束时间（天气-时辰）
	bool threeHourCityWeather;	//城市天气三小时预报标记,否则根据城市名或id查询天气
	std::string weatherhost;	//天气预报API接口网址
	std::string weatherpath;	//天气预报API接口路径
	int weatherport;			//天气预报API接口端口
	std::string weathercity;	//天气预报API接口指定城市
	std::string weatherauthorization;	//天气预报API接口授权
	int weatherUpTime;			//接口访问间隔时间（分钟）
};

struct UpdateConf
{
	UpdateConf()
	: timeUpFunc(false)
    , timeUpInterval(10)
    , waringEventFunc(false)	
    , verificationFunc(false)
    , timeCheckInterval(60)
	{

	};
	bool timeUpFunc;			//不变化或不刷新的数据定时推送功能开关
	int timeUpInterval;			//数据上送间隔
	//分析告警
	bool waringEventFunc;		//分析告警开关
	//下控校验
	bool verificationFunc;		//下控校验开关
	int timeCheckInterval;		//定时巡检数据变化或刷新的间隔
};

struct MqttConf
{
	MqttConf()
		: mqttFunc(false)
		, ip("127.0.0.1")
		, port(60009)
		, user("pyfree")
		, password("a12345")
		, json_flag(true)
		, hmac_sha_func(false)
		, hmac_sha_key("b12345")
	{

	};
	bool mqttFunc;			//mqtt服务接口开关
	std::string ip;			//服务的链接地址信息（Json)
	int port;				//端口
	std::string user;		//用户名
	std::string password;	//密码
	bool json_flag;			//true:json格式通信,false:acsii格式通信
	bool hmac_sha_func;		//
	std::string hmac_sha_key;//
};

struct BusinessConf
{
	BusinessConf()
	: devPath("dev.xml")
    , pmapPath("pmap.xml")
    , planFunc(true)
    , planPath("plan.xml")
    , alarmFunc(false)
    , alarmPath("alarm.xml")
	{

	};
    //xml_path
	std::string devPath;	//设备配置
	std::string pmapPath;	//下级到设备的数据映射配置
	//
	bool planFunc;			//任务调度开关
	std::string planPath;	//任务策略配置文件
	//
	bool alarmFunc;			//告警策略开关
	std::string alarmPath;	//告警策略配置文件
};

struct LocalLinkConf
{
	LocalLinkConf()
	: gather_ip("127.0.0.1")
	, gather_port(60002)
    , tranFunc(false)
    , ipclient("127.0.0.1")
    , portClient(60003)
    //本地socket通信
    , localSocketFunc(false)
	, ipLocalSocket("127.0.0.1")
    , portLocalSocket(60008)
	{

	};
	//侦听采集服务的网口ip
	std::string gather_ip;
	//侦听采集服务网络端口
	int gather_port;
	//链接第三方信息
	bool tranFunc;			//功能开关
	std::string ipclient;	//地址
	int portClient;			//端口
	//
	bool localSocketFunc;	//本地TCP/IP-Socket监控终端通信功能开关
	std::string ipLocalSocket;		//侦听网口
	int portLocalSocket;	//侦听端口
};

//后续需要重构,将各个功能模块涉及参数结构化,并配置文件(xml)里作为一个子节点集写入
struct AppConf
{
	AppConf()	
	{

	};
	//近地通信配置
    LocalLinkConf lconf;         //
    //系统辅助功能配置
    SysAssistConf saconf;   //
	//记录接口参配
	RecordConf	recconf;	//
	//告警推送接口参配
	WaringPushConf wpconf;	//
    //
    BusinessConf bsconf;    //
	//area info
    AreaInfo ainfo;             //
	//
    UpdateConf uconf;           //
	//聚合天气
    HttpWeather hweather;       //
	//
	AliyunConf aliconf;         //
	//
	MqttConf mqttconf;			//
};
};

#endif