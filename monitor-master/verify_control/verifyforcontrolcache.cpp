#include "verifyforcontrolcache.h"

#include <time.h>
#ifdef __linux__
#include <stdlib.h>
#endif
#include "business_def.h"
#include "Log.h"

float  VerifyForControlCache::VALLIMIT = float(0.0000001);
VerifyForControlCache* VerifyForControlCache::instance = NULL;
VerifyForControlCache* VerifyForControlCache::getInstance()
{
	if (NULL == VerifyForControlCache::instance)
	{
		VerifyForControlCache::instance = new VerifyForControlCache();
	}
	return VerifyForControlCache::instance;
}

VerifyForControlCache::VerifyForControlCache()
{
	init();
};
void VerifyForControlCache::Destroy()
{
	if (NULL != VerifyForControlCache::instance) 
	{
		delete VerifyForControlCache::instance;
		VerifyForControlCache::instance = NULL;
	}
}

VerifyForControlCache::~VerifyForControlCache()
{
	this->Destroy();
}

void VerifyForControlCache::init()
{
	ptr_CacheDataObj = BusinessDef::getInstance();
	queueforwar = QueueDataSingle<EventForWaring>::getInstance();
	queueforwar->setQueueDesc("event_war_queue");
}

void VerifyForControlCache::addVerifyData(VerificationCache it)
{
	m_MutexVerify.Lock();
	VerifyData.push_back(it);
	m_MutexVerify.Unlock();
}

void VerifyForControlCache::checkVerifyData()
{
	m_MutexVerify.Lock();
	std::list<VerificationCache>::iterator it = VerifyData.begin();
	while (it != VerifyData.end())
	{
		if (it->limitTimeForCheck < time(NULL)) 
		{
			//waring
			//告警描述
			EventForWaring event_;
			if (1 == it->pType || 3 == it->pType) 
			{
				event_.type = YKOpFail;
			}
			else
			{
				event_.type = YTOpFail;
			}
			event_.grade	= majorLevel;
			event_.send_	= it->eway_;
			event_.execTime = it->execTime;
			event_.taskID	= it->taskID;
			event_.taskDesc = "PlanOrUserControl";
			event_.devID	= it->devID;
			event_.devDesc	= it->devDesc;
			event_.pID		= it->pID;
			event_.pDesc	= it->pDesc;
			//值状况描述,阿里短信接口限定变量字段20以内
			char buf_val[64] = { 0 };
			sprintf(buf_val, "E(%.1f),A(%.1f)", it->val, it->val_);
			event_.valDesc = std::string(buf_val);
			//全状况描述
			char buf_comment[256] = { 0 };
			sprintf(buf_comment
				, "VerificationForControl taskID(%ld),taskDesc(%s)"
				",devID(%ld),devDesc(%s),pID(%ld),pDesc(%s),pType(%d)"
				",expect_value(%.2f),actual_value(%.2f)"
				, it->taskID, it->taskDesc.c_str()
				, it->devID, it->devDesc.c_str()
				, it->pID, it->pDesc.c_str(), it->pType
				, it->val, it->val_);
			event_.Comment = std::string(buf_comment);

			queueforwar->add(event_);
			//CLogger::createInstance()->Log(eTipMessage, "%s", buf_comment);
			it = VerifyData.erase(it);//time over		
			continue;
		}
		float _val = 0.0;
		if (ptr_CacheDataObj->getValue(it->devID, it->pID, _val))
		{
			it->val_ = _val;
			if (comFloatVal(_val, it->val)) 
			{
				it = VerifyData.erase(it);//value is map to be expected by control
				continue;
			}
		}
		it++;
	}
	m_MutexVerify.Unlock();
}

bool VerifyForControlCache::comFloatVal(float valA, float valB)
{
	return abs(valA - valB) < VALLIMIT;
}
