#include "socket_srv_acl.h"

#include "socket_private_acl.h"
#include "Log.h"

#ifdef WIN32
#define usleep(x) Sleep(x)
#endif

SocketSrv_ACL::SocketSrv_ACL(void)
	: running(true)
	, socket_acl(NULL)
{
}

SocketSrv_ACL::~SocketSrv_ACL(void)
{
	running = false;
}

void SocketSrv_ACL::setPDataPtr(SocketPrivate_ACL *socket_acl_)
{
	if (NULL != socket_acl_)
	{
		socket_acl = socket_acl_;
	}
};

void* SocketSrv_ACL::run()
{
	if (NULL==socket_acl)
	{
		CLogger::createInstance()->Log(MsgInfo,
			"MySocketSrv start fail for myPDataPrt is NULL"
			",[%s %s %d]!"
			, __FILE__, __FUNCTION__, __LINE__);
		return 0;
	}
	while(running)
	{
		socket_acl->Accept();
		usleep(10);
	}
	return NULL;
}
