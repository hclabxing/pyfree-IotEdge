#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_PRIVATE_ACL_H_
#define _SOCKET_PRIVATE_ACL_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_private_acl.h
  *File Mark       : 
  *Summary         : TCP-SOcket服务处置类
  *基于第三方库(acl)开发的TCP-Socket通信接口,提供了信道的连接、关闭、接入侦听、数据读写等操作
  *连接客户端将加入Map容器进行标记,方便针对各个连接独立处理
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include<map>

#include "Mutex.h"
#include "hashmap.h"
#include "datadef.h"

#include "acl_cpp/lib_acl.hpp"


struct ClientSOCKET_ACL
{
	acl::socket_stream* client;
	int flag;
};

class SocketPrivate_ACL
{
public:
	/**
	 * 构造函数
	 * @param ip {string} 网络地址
	 * @param port {uint} 端口
	 * @return {}
	 */
	SocketPrivate_ACL(unsigned int port,std::string ip="0.0.0.0");
	~SocketPrivate_ACL();
public:
	/**
	 * 网络连接,打开tcp-sokcet接口
	 * @return {int} 0失败,1成功
	 */
	int onConnect();
	/**
	 * 断开网络连接,主动关闭所有连接进来的各个客户端连接和本地连接,释放资源
	 * @return {int} 0失败,1成功
	 */
	void disConnect();
	/**
	 * 关闭指定客户端,释放资源
	 * @param client_acl {acl::socket_stream*} 客户端连接实例
	 * @return {void}
	 */
	void close_client(acl::socket_stream* client_acl);
	/**
	 * 是否存在连接进来的客户端
	 * @return {bool}
	 */
	bool emptyClients();
	/**
	 * 从客户端列表中获取新添加的客户端信息
	 * @param ips {vector} 新连接的客户端网络地址集
	 * @return {bool}
	 */
	bool getNewAddClient(std::vector<unsigned long long> &ips);
	/**
	 * 从各个客户端读取数据并存储在其对应的缓存中
	 * @param bufs {map} 读取内容返回
	 * @return {int} <0异常,>0本次读取内容大小(所有客户端)
	 */
	int Read(std::map<KeyObj_Client, RDClient> &bufs);
	/**
	 * 向所有客户端写入数据
	 * @param buf {congst char*} 写入内容指针
	 * @param size {int}  写入内容大小
	 * @return {int} <=0异常,>0实际写入内容大小
	 */
	int Write(const char* buf, int size);
	/**
	 * 向指定客户端写入数据
	 * @param ipInt {ulonglong} 客户端网络地址
	 * @param buf {congst char*} 写入内容指针
	 * @param size {int}  写入内容大小
	 * @return {int} <=0异常,>0实际写入内容大小
	 */
	int Write(unsigned long long ipInt, const char* buf, int size, bool &mapIpF);

	bool Accept();
private:
	acl::server_socket		m_SSocket;			//服务端
	acl::string				addr;				//网络连接信息(ip:port)
	unsigned int			m_Port;				//端口变量
	bool					m_OnListen;			//用于标注侦听
	PYMutex					m_MyMutex;			//线程锁
	std::map<KeyObj_Client, ClientSOCKET_ACL>		m_CSockets;			//绑定客户端
	bool					newlinkF;			//新链接标记,为降低巡检而设
};

#endif
