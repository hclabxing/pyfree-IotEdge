#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCK_LOCAL_WRITE_H_
#define _SOCK_LOCAL_WRITE_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_loacl_write.h
  *File Mark       : 
  *Summary         : 
  *该线程向本地客户端（域内）写入数据,将数据协议编码并序列化
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"
#include "datadef.h"
#include "queuedata_single.h"

class SocketPrivate_ACL;
class BusinessDef;

class SocketLocalWrite : public acl::thread
{
public:
	SocketLocalWrite(void);
	~SocketLocalWrite(void);
  /**
	 * 设置通信接口指针
	 * @param socket_acl_ {SocketPrivate_ACL* } 通信接口,基于acl库实现
	 * @return {void}
	 */
	void setPrivateDataPtr(SocketPrivate_ACL *socket_acl_);
	void* run();
private:
  /**
	 * 获取的设备初始信息,在每次重连时,全面推送一次设备信息及其下点信息,调用devInfoUp和pointInfoUp进行信息推送
	 * @return {void}
	 */
	void InitDev();
  /**
	 * 推送设备信息
	 * @param _ipInt {ulonglong }  网络地址整型表达
   * @param devID {int } 设备编号
   * @param devType {int } 设备类型
   * @param desc {string } 设备描述(名称)
	 * @return {void}
	 */
	void devInfoUp(unsigned long long _ipInt, int devID,int devType, std::string desc);
  /**
	 * 推送点信息
	 * @param _ipInt {ulonglong }  网络地址整型表达
   * @param devID {int } 设备编号
   * @param pID {int } 点编号
   * @param pType {int } 点类型
   * @param desc {string } 点描述(名称)
   * @param defval {float } 数值
	 * @return {void}
	 */
	void pointInfoUp(unsigned long long _ipInt, int devID, int pID, int pType
		, std::string desc, float defval);
  /**
	 * 从待写入客户端的缓存队列中获取到点态势信息,调用pointInfoUp进行推送
	 * @return {void}
	 */
	void PValueUp();
  /**
	 * 从待写入客户端的缓存队列中获取到点态势信息,并生成推送内容
   * @param _ipInt {int } 输出整型格式网络地址
   * @param _buf {uchar* } 输出推送内容
	 * @return {int} <0异常,>0推送内容大小
	 */
	int getBuffer(unsigned long long &_ipInt, unsigned char* _buf);
private:
	bool running;       //线程运行标记
	SocketPrivate_ACL 	*socket_acl; //通信接口
	QueueDataSingle<SocketLocalWriteItem> *queueforwrite; //待推送点态势队列
	BusinessDef			*m_CacheDataObj;        //业务信息,单体类实例
};

#endif

