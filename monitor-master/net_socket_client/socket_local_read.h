#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_LOCAL_READ_H_
#define _SOCKET_LOCAL_READ_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_loacl_read.h
  *File Mark       : 
  *Summary         : 
  *该线程向本地（域内）客户端读取数据,并将数据分帧及帧解析
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "datadef.h"
#include "queuedata.h"
#include "queuedata_single.h"

class SocketPrivate_ACL;
class BusinessDef;
class VerifyForControlCache;

class SocketLocalRead : public acl::thread
{
public:
	SocketLocalRead(void);
	~SocketLocalRead(void);

  /**
	 * 设置通信接口指针
	 * @param socket_acl_ {SocketPrivate_ACL* } 通信接口,基于acl库实现
	 * @return {void}
	 */
	void setPrivateDataPtr(SocketPrivate_ACL *socket_acl_);
	void* run(); 
private:
  /**
	 * 若是采集点,获取对应的采集点信息,生成下控信息写入待下发采集端的缓存队列,同时下控预期结果进行校验信息写入缓存队列
   * 若是虚拟点,则直接进行设值操作
   * @param devID {int } 设备编号
   * @param pID {int } 点编号
   * @param val {float } 值
	 * @return {void}
	 */
	void setPValue(int devID, int pID, float val);
  /**
	 * 帧数据解析
   * @param link {string } 网络地址
   * @param buf {congst uchar* } 帧数据
   * @param len {int } 帧大小
	 * @return {int} <0异常,>0成功
	 */
	int AddFrame(const std::string link, const unsigned char *buf, int len);
private:
	bool running;       //线程运行标记
	SocketPrivate_ACL 	*socket_acl; //通信接口
	BusinessDef			*ptr_bdef;       //业务信息,单体类实例
	bool is_Verification;            //是否需要对客户端端下控预期结果进行校验
	VerifyForControlCache *ptr_vcc;  //预期校验信息缓存,单体类实例
private:
	QueueData<DataFromChannel> *read_datas;            //从客户端读取的缓存数据
	QueueDataSingle<DataToGather> *socket_write_queue; //待下发采集端的缓存数据
};

#endif