#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef _DELAY_PLAN_THREAD_H_
#define _DELAY_PLAN_THREAD_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : delay_plan_thread.h
  *File Mark       : 
  *Summary         : 
  *1)从任务计划线程迁出,防止进行执行命令等待时延误了已经加入列表的延时指令的执行
  *2)在配置任务策略中,如果不是必须,最好不要设置等待时间,而设置延时时间,满足条件的指令会加入本线程的业务队列
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "datadef.h"
#include "queuedata_single.h"

class BusinessDef;

class DelayPlanThread : public acl::thread
{
public:
	DelayPlanThread(void);
	~DelayPlanThread(void);
	void* run();

	void addDelayExec(DataToGather wd);
protected:

private:
	//
private:
	bool running;
	BusinessDef* ptr_bdef;

	std::list<DataToGather> delay_plan_list;
	PYMutex delay_plan_list_Mutex;
	QueueDataSingle<DataToGather> *socket_write_queue;
};
#endif
