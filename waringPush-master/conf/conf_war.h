#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _CONF_WAR_H_
#define _CONF_WAR_H_
/***********************************************************************
  *Copyright 2020-06-13, pyfree
  *
  *File Name       : conf_war.h
  *File Mark       : 
  *Summary         : 告警信息配置
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>

#include "dtypedef.h"

using namespace pyfree;

//事件信息
struct EventForWaring
{
	//默认构造
	EventForWaring()
		: send_(AlarmForDef)
		, execTime("")
		, area_desc("py")
		, desc("DefAlarmType")
		, levelDesc("DefLevel")
		, taskDesc("UnDefTask")
		, devDesc("UnDefDev")
		, pDesc("UnDefPoit")
		, valDesc("")
		, code_xf("")
		
	{

	};
	EventForWaring& operator = (const EventForWaring& rhs) //赋值符重载  
	{
		if (this == &rhs)
		{
			return *this;
		}
		send_ = rhs.send_;
		execTime = rhs.execTime;
		area_desc = rhs.area_desc;
		desc = rhs.desc;
		levelDesc = rhs.levelDesc;
		taskDesc = rhs.taskDesc;
		devDesc = rhs.devDesc;
		pDesc = rhs.pDesc;
		valDesc = rhs.valDesc;
		code_xf = rhs.code_xf;

		return *this;
	};
	EventWay	send_;		//通知方式
	std::string execTime;	//时间
	std::string area_desc;	//地区
	std::string desc;		//事件描述
	std::string levelDesc;  //等级描述
	std::string taskDesc;	//任务描述
	std::string devDesc;	//设备描述
	std::string pDesc;		//点描述
	std::string valDesc;	//值状况描述
	std::string code_xf;	//
	
};

#endif
