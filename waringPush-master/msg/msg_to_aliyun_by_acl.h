#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef _MSG_TO_ALIYUN_ACL_H_
#define _MSG_TO_ALIYUN_ACL_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : msg_to_aliyun_by_acl.h
  *File Mark       : 
  *Summary         : 基于第三方库acl实现的阿里云短信接口
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "dtypedef.h"
#include "conf_war.h"

struct HttpMsgArg
{
	HttpMsgArg()
		: startTime(0)
		, endTime(1440)
		//
		, addr_("dysmsapi.aliyuncs.com")
		, path_("")
		, port_(80)
		//
		, AccessKeyId("LTAIWGIhMW9lfJNH")
		, AccessKeySecret("aBbus0elERjdtScn6TgxzZhaNilYjC")
		, Timestamp("2018-04-13T10:10:10Z")
		, Format("XML")
		, SignatureMethod("HMAC-SHA1")
		, SignatureVersion("1.0")
		, SignatureNonce("1")
		, Signature("")
		//
		, Action("SendSms")
		, Version("2017-05-25")
		, RegionId("cn-hangzhou")
		, PhoneNumbers("13726272065")
		, SignName("短信测试")
		, TemplateCode("SMS_130929354")
		, TemplateParam("")
		, SmsUpExtendCode("")
		, OutId("123")
	{

	};
	//
	unsigned int startTime;				//开始告警信息时间（单位分钟）
	unsigned int endTime;				//结束告警信息时间（单位分钟）
	//////////////////////////////////////*短信API接口参数集*///////////////////////////////////////////
	std::string addr_;			// web 服务器地址  
	std::string path_;			// 本地请求的数据文件  
	int port_;
	//
	std::string AccessKeyId;		//
	std::string AccessKeySecret;	//
	std::string Timestamp;			//格式为：yyyy-MM-dd’T’HH:mm:ss’Z’；时区为：GMT
	std::string Format;				//没传默认为JSON，可选填值：XML
	std::string SignatureMethod;	//建议固定值：HMAC-SHA1
	std::string SignatureVersion;	//建议固定值：1.0
	std::string SignatureNonce;		//用于请求的防重放攻击，每次请求唯一，JAVA语言建议用：java.util.UUID.randomUUID()生成即可
	std::string Signature;			//最终生成的签名结果值

	std::string Action;				//API的命名，固定值，如发送短信API的值为：SendSms
	std::string Version;			//API的版本，固定值，如短信API的值为：2017-05-25
	std::string RegionId;			//API支持的RegionID，如短信API的值为：cn-hangzhou
	/*
	*短信接收号码,支持以逗号分隔的形式进行批量调用，
	*批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,
	*验证码类型的短信推荐使用单条调用的方式
	*/
	std::string PhoneNumbers;
	std::string SignName;			//短信签名
	std::string TemplateCode;		//短信模板ID
	/*
	短信模板变量替换JSON串,友情提示:如果JSON中需要带换行符,请参照标准的JSON协议。
	*/
	std::string TemplateParam;
	std::string SmsUpExtendCode;	//上行短信扩展码,无特殊需要此字段的用户请忽略此字段
	std::string OutId;				//外部流水扩展字段
};

namespace acl
{
	class http_request;
};

class MsgToAliyun
{
public:
    MsgToAliyun();
    ~MsgToAliyun();

    /**
	 * 将告警信息生成短信内容并发送到阿里云短信服务中心
	 * @return {bool}
	 */
    void sendMsg(EventForWaring efw);
    /**
	 * 根据短信配置信息指定的时间范围判定当前是否可以发送短信
	 * @return {bool}
	 */
    bool isMapMsgTime();
private:
    void init();
    /**
	 * 向阿里云短信服务中心发送短信
     * @param templateParam_ {string } 短信描述信息
	 * @return {void}
	 */
    void send_msg(std::string templateParam_);
    bool do_plain(acl::http_request& req);
	//bool do_xml(acl::http_request& req);
	bool do_json(acl::http_request& req);
private:
    HttpMsgArg myArgHttp;
};

#endif
