#ifndef SOCKETSERVER_H
#define SOCKETSERVER_H

#include <QObject>
#include <QStringList>

QT_BEGIN_NAMESPACE
class QTcpServer;
class QTcpSocket;
QT_END_NAMESPACE

class SocketServer : public QObject
{
	Q_OBJECT
public:
	SocketServer();
	~SocketServer();


private:
	void init();
	bool isListen();
	void stopListen();
    int code(const unsigned char *buff, const int len, unsigned char *outbuf);
	int uncode(const unsigned char *buff, int len, unsigned char *outbuf);
signals:
    void SetItem(int _idx, int _val);
public slots:
    void sendItem(int id,int val);
private slots:
	void newConnectionSlot();
    void disObj();
	void dataReceived();
private:
    int listenPort;
	QTcpServer *m_pServer;
 	QTcpSocket *m_pSocket;
};

#endif
