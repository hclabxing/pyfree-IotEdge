/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QApplication>
//#include <QGraphicsView>
//#include <QGraphicsProxyWidget>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QtGlobal>
#include <QTranslator>
#include <QTextCodec>
#include <QString>
#include <QDebug>

#include "player.h"
#include "socketServer.h"
#include "routing.h"

#define LOGFILEMAX 10000
void logMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    //加载日志模块
    qInstallMessageHandler(logMsgHandler);

    //languages translation, main event is translate Marco tr("")
    QTranslator translator;
    //装载翻译文件
    //lupdate *.pro导出源文代码
    translator.load(":/languages/player_cn.qm");
    app.installTranslator(&translator);
    //设置本地语言
    QTextCodec::setCodecForLocale(QTextCodec::codecForLocale());
//    //设置UTF-8
//    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
//    QTextCodec::setCodecForLocale(codec);

    QCoreApplication::setApplicationName(QObject::tr("player for zhsye"));
    QCoreApplication::setOrganizationName(QObject::tr("Zhuhai Singyes New Mstar Technology Ltd"));
    QCoreApplication::setApplicationVersion(QT_VERSION_STR);
    QCommandLineParser parser;
    parser.setApplicationDescription(QObject::tr("matching player software in zhsye monitor system"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("url", "The URL to open.");
    parser.process(app);

    Player player;

    if (!parser.positionalArguments().isEmpty() && player.isPlayerAvailable()) {
        QList<QUrl> urls;
        foreach (const QString &a, parser.positionalArguments())
            urls.append(QUrl::fromUserInput(a, QDir::currentPath(), QUrl::AssumeLocalFile));
        player.addToPlaylist(urls);
    }

//#ifdef ANDROID
//    QGraphicsScene *scene = new QGraphicsScene;
//    QGraphicsProxyWidget *w = scene->addWidget(&player);
//    w->setRotation(90);
//    QGraphicsView *view = new QGraphicsView(scene);
////    view->resize(810, 610);
//    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
//    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
//    view->setFrameShape(QFrame::NoFrame);
//    view->setFrameShadow(QFrame::Plain);
//    view->show();
//#else
#if defined(Q_WS_SIMULATOR)
    player.setAttribute(Qt::WA_LockLandscapeOrientation);
    player.showMaximized();
#else
    player.show();
#endif
//#endif
    SocketServer *socketSrv = new SocketServer();
    QObject::connect(socketSrv, SIGNAL(SetItem(int, int))
        ,&player, SLOT(control(int,  int)));
    Routing *rout = new Routing(&player);
    QObject::connect(rout, SIGNAL(sendItem(int, int))
        ,socketSrv, SLOT(sendItem(int,  int)));
    QObject::connect(&player, SIGNAL(notify())
        ,rout, SLOT(Tolcall()));
    rout->start();

    return app.exec();
}

void logMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
//    QByteArray localMsg = msg.toLocal8Bit();
    QString typeStr = "InfoMsg";
    switch (type) {
        case QtDebugMsg:
            typeStr = "Debug";
            break;
        case QtWarningMsg:
            typeStr = "Warn";
            break;
        case QtCriticalMsg:
            typeStr = "Critical";
            break;
        case QtFatalMsg:
            typeStr = "Fatal";
            break;
        default:
            break;
    }
    QString strLog = QString(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss ")+"[%1] %2 (%3:%4,%5)")
                .arg(typeStr).arg(/*localMsg.constData()*/msg).arg(context.file).arg(context.line).arg(context.function);
    QFile outFile("debug.log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);

    /**< the max size of log.txt.*/
    if(outFile.size()/100>LOGFILEMAX)
    {
        outFile.close();
        outFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Truncate);
        outFile.close();
        outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    }

    QTextStream ts(&outFile);
    ts << strLog << endl;
};
