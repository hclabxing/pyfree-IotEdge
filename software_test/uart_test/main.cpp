#include "uart_unix.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	int fd;
	int ret;
	char r_buf[256];
	bzero(r_buf,256);
	fd = pyfree::uart_open(fd, "/dev/ttyS0");//选择的是/dev/ttyS0串口
	if(fd == -1)
	{
		fprintf(stderr,"open failed!\n");
		exit(EXIT_FAILURE);
	}
	fprintf(stderr,"open success!\n");
	if(pyfree::uart_config(fd,19200,'N',8,'N',1) == -1)
	{
		fprintf(stderr,"configure failed!\n");
		exit(EXIT_FAILURE);
	}
	fprintf(stderr,"config success!\n");
	pyfree::uart_clear(fd,TCIOFLUSH);
	while (1) {
		ret = pyfree::uart_read(fd,r_buf,256,1000);                
		if(ret == -1)
		{
			fprintf(stderr, "uart_read failed!\n");
			exit(EXIT_FAILURE);
		}
		if(ret>0){
			printf("buf:%s\n", r_buf);
			// ret = pyfree::uart_write(fd,r_buf,strlen(r_buf));   
			// if(ret == -1)
			// {
			// 	fprintf(stderr, "uart_write failed!\n");
			// 	exit(EXIT_FAILURE);
			// }
		}
	}

	ret = pyfree::uart_close(fd);
	if(ret == -1)
	{
		fprintf(stderr, "close failed!\n");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}
