/****************************************************************************

****************************************************************************/

#ifndef TREEITEM_HPP
#define TREEITEM_HPP

#include <QList>
#include <QVariant>
#include <QVector>

class TreeItem
{
public:
    TreeItem(const QVector<QVariant> &data,
        TreeItem *parent = 0);
    ~TreeItem();

    TreeItem *child(int number);
	bool isEmpty();
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    bool insertChildren(int position, int count, int columns);
    bool insertColumns(int position, int columns);
    TreeItem *parent();
    bool removeChildren(int position, int count);
    bool removeColumns(int position, int columns);
    int childNumber() const;
    bool setData(int column, const QVariant &value);

    bool contains ( const QVariant & treeValue ) const;
    int indexChild(const QVariant & treeValue) const;

    void setIcon(QVariant ic);
    QVariant getIcon();
    void setState(QVariant st);
    QVariant getState();
    bool isChecked();
    void setCheckState(bool state);
private:
    QList<TreeItem*> childItems;
    QVector<QVariant> itemData;
    TreeItem *parentItem;
    QVariant _icon;
    QVariant _state;
    bool m_checked;
};

#endif
