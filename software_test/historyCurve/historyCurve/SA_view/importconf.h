#ifndef IMPORTCONF_HPP
#define IMPORTCONF_HPP

#include <QDialog>
#include <QMap>
#include <QList>
#include <QStringList>

QT_BEGIN_NAMESPACE
class QWidget;
class QMouseEvent;
class QPushButton;
class QComboBox;
class QLabel;
class QLineEdit;
QT_END_NAMESPACE

 struct castInfo
 {
 	int  column; //列
 	QString wildcard;//通配信息
 };

struct ImportConfInfo
{
	QString file;//文件
	QStringList nodes;//xml节点名
	QString div;//分割符
	int start;	//初始行号
	// QMap<int,int> list;//列映射
	QMap<int,castInfo> list;//列映射
};

class ImportConf : public QDialog
{
	Q_OBJECT
public:
	ImportConf(QStringList _list,QWidget * parent = 0,int _fileDialogF=-1,Qt::WindowFlags f = 0);
	~ImportConf();
protected:
	// void mousePressEvent ( QMouseEvent * event );
signals:
	void sendConfInfo(ImportConfInfo inconf);
private slots:
	void setImportInfo();
	void getFilePath();
private:
	void win_init();
	void data_init(QStringList _list);
private:
	int fileDialogF;
	QLineEdit *file_LineEdit;
	QLabel *xml_label;
	QLineEdit *xmlNode_LineEdit;
	QLineEdit *div_LineEdit;
	QPushButton *filebrowse_Button;
	QLineEdit *start_LineEdit;
	QPushButton *okButton;
    QPushButton *cancelButton;
    QList<QLineEdit*> _checklist;

    static const int fixedlabelW = 96;
    static const int fixedEditW = 96;
    static const int fixedButtonW = 64;
};
#endif //IMPORTCONF_HPP