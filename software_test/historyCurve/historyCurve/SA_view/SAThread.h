#ifndef SAThread_H
#define SAThread_H

#include <QThread>
#include <QMap>
#include <QMutex>

#include "SA_db/SADef.h"

QT_BEGIN_NAMESPACE
class SADB;
QT_END_NAMESPACE

class SAThread : public QThread
{
	Q_OBJECT
public:
	SAThread(QObject * parent = 0);
	~SAThread();

	bool openDB(QString _path);
	bool loadDBConf(long long &_startT,long long &_endT);
	bool getNewTimeRange(long long &_startT,long long &_endT);
	bool loadDBData(long long _startT,long long _endT,QSet<int> &_devids,QSet<int> &_idxs);
	bool getDrawData(QMap<QString,QMap<QDateTime,qreal> > &_draws
		,QMap<QString,LimitData> &val_limit,int _devid,int _idx=-1);
	// bool getSADataQuery(QList<SADataItem> &_datas);

	void startRead();
	void stopRead();
protected:
    void run();
signals:
	void newItem(QList<SADataItem> _data);
public:
    QList<SADataItem> _datas;
    QMutex data_mutex;
private:
	static const int saDSize = 100000;
	bool runFlag;
	QString drDir;
	SADB *ptr_SADB;
	qlonglong sa_lasttime;//agc data item last time in table
	qlonglong sa_lordertime;//user order time
};

#endif //SAThread_H