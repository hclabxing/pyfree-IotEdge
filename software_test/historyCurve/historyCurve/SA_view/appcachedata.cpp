#include "appcachedata.h"

AppCacheData* AppCacheData::instance = NULL;
AppCacheData* AppCacheData::getInstance()
{
    if(NULL == AppCacheData::instance)
    {
        AppCacheData::instance = new AppCacheData();
    }
    return AppCacheData::instance;
}

void AppCacheData::Destroy()
{
	if(NULL!=AppCacheData::instance){
		delete AppCacheData::instance;
		AppCacheData::instance = NULL;
	}
}

AppCacheData::AppCacheData()
{
	init();
}

AppCacheData::~AppCacheData()
{
}

void AppCacheData::init()
{
	xmlnodes="";
	linediv=",";
}

QString AppCacheData::getXmlNodes()
{
	return xmlnodes;
}

void AppCacheData::setXmlNodes(QString _nodes)
{
	xmlnodes=_nodes;
}

QString AppCacheData::getLineDiv()
{
	return linediv;
}

void AppCacheData::setLineDiv(QString _div)
{
	linediv=_div;
}