/***********************************************************************
*Copyright (c) 2011 Easy Communication Electtrical Power Tech.Co.Ltd
*
*File Name       : mainwindow.h
*File Mark       :
*Summary         : the app main view
*
*Current Version : 1.00
*Author          : PengYong
*FinishDate      :
*
*Replace Version :
*Author          :
*FinishDate      : 2016-12-15

************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>

QT_BEGIN_NAMESPACE
class QCloseEvent;
class QAction;
class QMenu;

class MyMenuBar;
class MyMenu;
class MyToolBar;
class MyStatusBar;

class QWidget;
class SAView;
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();
protected:
	#ifdef _MIN_WIN_
	void closeEvent(QCloseEvent *event);
	void changeEvent ( QEvent * event );
	#endif
	// void mouseMoveEvent ( QMouseEvent * event );
	// void mousePressEvent( QMouseEvent * event );
	// void mouseReleaseEvent( QMouseEvent * event );
private:
	void init();
	void createActions();
    void createMenus();
    void createToolBars();
    void createStatusBar();
    #ifdef _MIN_WIN_
	void initTray();
	#endif
	void setFontFormat();
	#ifdef _MIN_WIN_
	bool closeEventEnsure();
	#endif
	void infoSvaeEventEnsure();
signals:

public slots:
	void showMsg(QString _msg);
private slots:
	void aboutApp();
	#ifdef _MIN_WIN_
	void trayiconActivated(QSystemTrayIcon::ActivationReason reason);
	#endif
private:
	SAView *view;
	//
	MyMenuBar *m_MyMenuBar;
	//menu
	MyMenu *fileMenu;
	MyMenu *helpMenu;

	//toolbar
    MyToolBar *fileToolBar;
    MyToolBar *aboutToolBar;
    //StatusBar
    MyStatusBar *m_MyStatusBar;

	QAction *exitAct;
	QAction *aboutAct;

	///
	QSystemTrayIcon *trayIcon;
    QAction *minimizeAction;
    QAction *restoreAction;
    QAction *helpAction;
    QAction *quitAction;
    QMenu   *trayIconMenu;
};

#endif // MAINWINDOW_H
