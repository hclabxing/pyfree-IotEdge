#include "dataitem.h"
#include "pfunc_qt.h"

#include <QObject>
#include <stdio.h>

DataItem::DataItem()
    : data()
    , name("")
{
    init();
}

DataItem::~DataItem()
{
	if(!data.isEmpty())
		data.clear();
}

DataItem& DataItem::operator=(const DataItem& rhs)
{
    if(this!=&rhs){
        this->name = rhs.name;
        this->data = rhs.data;
    }
    return *this;
}

void DataItem::init()
{

}

void DataItem::append(rowData value)
{
    data.append(value);
}

void DataItem::append(QDateTime dt, qreal d, unsigned char _quality)
{
    data.append(rowData(dt.date(),dt.time(),d,_quality));
}

void DataItem::append(QDate ds,QTime t, qreal d, unsigned char _quality)
{
    data.append(rowData(ds,t,d,-_quality));
}

void DataItem::append(QDate ds,QTime t, qreal d, unsigned char _quality,QString unit)
{
    data.append(rowData(ds,t,d,_quality,unit));
}

void DataItem::prepend(rowData value)
{
    data.prepend(value);
}
void DataItem::prepend(QDate ds,QTime t, qreal d, unsigned char _quality)
{
    data.prepend(rowData(ds,t,d,_quality));
}
void DataItem::prepend(QDate ds,QTime t, qreal d, unsigned char _quality,QString unit)
{
    data.prepend(rowData(ds,t,d,_quality,unit));
}

QList<rowData> DataItem::getData()
{
    return data;
}

QTime DataItem::getMinQTime()
{
    QTime minTime = data.value(0).getTime();
    for(int i=0; i<data.size(); i++)
    {
        if(minTime>data.value(i).getTime())
        {
            minTime = data.value(i).getTime();
        }
    }
    return minTime;
}

QTime DataItem::getMaxQTime()
{
    QTime maxTime = data.value(0).getTime();
    for(int i=0; i<data.size(); i++)
    {
        if(maxTime<data.value(i).getTime())
        {
            maxTime = data.value(i).getTime();
        }
    }
    return maxTime;
}

qreal DataItem::getMinData()
{
    if (data.isEmpty())
    {
        return 999999.0;
    }
    qreal minData = data.value(0).getData();
    for(int i=0; i<data.size(); i++)
    {
        if(minData>data.value(i).getData())
        {
            minData = data.value(i).getData();
        }
    }
    return minData;
}

qreal DataItem::getMaxData()
{
    if (data.isEmpty())
    {
        return 0.0;
    }
    qreal maxData = data.value(0).getData();
    for(int i=0; i<data.size(); i++)
    {
        if(maxData<data.value(i).getData())
        {
            maxData = data.value(i).getData();
        }
    }
    return maxData;
}

rowData DataItem::value(int i)
{
    return data.value(i);
}

bool DataItem::isEmpty()
{
    return data.isEmpty();
}

int DataItem::size()
{
    return data.size();
}

rowData DataItem::getLast()
{
    return data.last();
}

void DataItem::removeLast()
{
    data.removeLast();
}
//2014-03-04
void DataItem::setName(QString _n)
{
    name = _n;
}
QString DataItem::getName()
{
    return name;
}

QString DataItem::getDesc()
{
    QString ret = "";
    for(int i=0; i<data.size(); i++)
    {
        if(i==0){
            ret+=QObject::tr("accompanying-data:");
        }
        ret+=QString("[%1,%2,%3]")
            .arg(PFUNC_QT::ChangeDateTime(data.value(i).getDateTime()))
            .arg(data.value(i).getData())
            .arg(data.value(i).getDescribe());
        if(i!=(data.size()-1))
        {
            // ret+=QObject::tr("transform");
            ret+=",";
        }
    }
    return ret;
}
////////////////////////////////////////////////

rowData::rowData()
    : date(QDate(0,0,0)),time(QTime(0,0,0)), data(0), quality(0x00)
{
    changeDesc();
}

rowData::rowData(QDate ds,QTime t, qreal d, unsigned char _quality,QString unit)
    : date(ds), time(t), data(d), quality(_quality),dataUnit(unit)
{
    changeDesc();
}

rowData::rowData(const rowData& rValue)
{
    if(this != &rValue)
    {
		this->date = rValue.date;
        this->time = rValue.time;
        this->data = rValue.data;
        this->quality = rValue.quality;
        this->describe = rValue.describe;
    }
}

rowData& rowData::operator=(const rowData& rValue)
{
    if(this == &rValue)
    {
        return *this;
    }else{
		this->date = rValue.date;
        this->time = rValue.time;
        this->data = rValue.data;
        this->quality = rValue.quality;
        this->describe = rValue.describe;
        return *this;
    }
}

rowData::~rowData()
{

}

QDateTime rowData::getDateTime()
{
    return QDateTime(date,time);
}

QDate rowData::getDate()
{
	return date;
}

QTime rowData::getTime()
{
    return time;
}

qreal rowData::getData()
{
    return data;
}

QString rowData::getDataUnit()
{
    return dataUnit;
}

void rowData::changeDesc()
{
    try{
        if(0x00==quality)
        {
            describe = QString("");
            return;
        }
        describe = "[";
        char buf[32] = {0};
        sprintf(buf,"%02X",quality);
        describe += QString("%1").arg(buf);
        if(quality&0x01)
        {
            describe += QString(",");
            describe += QObject::tr("overflow");
            // describe += QString(",溢出");
        }
        if(quality&0x02)
        {
            describe += QString(",");
            describe += QObject::tr("block");
            // describe +=QString(",封锁");
        }
        if(quality&0x04)
        {
            describe += QString(",");
            describe += QObject::tr("replace");
            // describe +=QString(",替代");
        }
        if(quality&0x08)
        {
            describe += QString(",");
            describe += QObject::tr("non-present-value");
            // describe +=QString(",非现值");
        }
        if(quality&0x10)
        {
            describe += QString(",");
            describe += QObject::tr("invalid-value");
            // describe +=QString(",无效值");
        } 
        describe += "]";
    }catch(...){
        describe = QString("");
    }
}

unsigned char rowData::getQDS()
{
    return quality;
}

QString rowData::getDescribe()
{
    return describe;
}
