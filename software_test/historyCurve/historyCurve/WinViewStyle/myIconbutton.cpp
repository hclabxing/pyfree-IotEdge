#include "myIconbutton.h"

MyIconButton::MyIconButton(QWidget * parent)
    : QPushButton(parent)
{
    init();
}

MyIconButton::MyIconButton(const QString & text, QWidget * parent)
    : QPushButton(text,parent)
{
    init();
}

MyIconButton::MyIconButton(const QIcon & icon, const QString & text, QWidget * parent)
    : QPushButton(icon,text,parent)
{
    init();
}

MyIconButton::~MyIconButton()
{

}

void MyIconButton::init()
{
    this->setIconSize(QSize(32,32));
    this->setMinimumSize(48,48);
    this->setMaximumSize(48,48);
    this->setCheckable(true);
}
