#ifndef MYMENU_H
#define MYMENU_H

#include <QMenu>

class MyMenu : public QMenu
{
public:
    MyMenu(QWidget * parent = 0);
    MyMenu(const QString & title, QWidget * parent = 0);
    ~MyMenu();
private:
    void init();
};

#endif // MYMENU_H
