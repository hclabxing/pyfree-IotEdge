#ifndef MYPROJECTBUTTON_H
#define MYPROJECTBUTTON_H

#include <QPushButton>

class MyIconButton : public QPushButton
{
public:
    MyIconButton(QWidget * parent = 0);
    MyIconButton(const QString & text, QWidget * parent = 0);
    MyIconButton(const QIcon & icon, const QString & text, QWidget * parent = 0);
    ~MyIconButton();
private:
    void init();
};

#endif // MYPROJECTBUTTON_H
