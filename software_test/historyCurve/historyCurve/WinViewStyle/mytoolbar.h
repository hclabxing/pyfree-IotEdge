#ifndef MYTOOLBAR_H
#define MYTOOLBAR_H

#include <QToolBar>
#include <QString>

class MyToolBar : public QToolBar
{
    Q_OBJECT
public:
    explicit MyToolBar(QWidget *parent = 0);
    explicit MyToolBar( const QString & title, QWidget * parent = 0 );
    ~MyToolBar();
private:
    void init();
signals:

public slots:

};

#endif // MYTOOLBAR_H
