#ifndef MYSTATUSBAR_H
#define MYSTATUSBAR_H

#include <QStatusBar>

class MyStatusBar : public QStatusBar
{
public:
    MyStatusBar();
    ~MyStatusBar();
private:
    void init();
};

#endif // MYSTATUSBAR_H
