#ifndef MYMENUBAR_H
#define MYMENUBAR_H

#include <QMenuBar>

class MyMenuBar : public QMenuBar
{
    Q_OBJECT
public:
    explicit MyMenuBar(QWidget *parent = 0);
    ~MyMenuBar();
private:
    void init();
signals:

public slots:

};

#endif // MYMENUBAR_H
