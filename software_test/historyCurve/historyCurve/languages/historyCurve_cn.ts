<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN">
<context>
    <name>CloseCheck</name>
    <message>
        <source>App close to confirm</source>
        <translation type="unfinished">程序关闭确认</translation>
    </message>
    <message>
        <source>Systerm is runing,do you want to close App now? 
</source>
        <translation type="unfinished">程序正在运行，你是否需要立刻关闭它？</translation>
    </message>
    <message>
        <source> Select OK is close App or select Cancel is unclose App. 
</source>
        <translation type="unfinished">选择‘确定’关闭程序，或选择‘取消’放弃关闭程序。</translation>
    </message>
    <message>
        <source>random text produced by computer:</source>
        <translation type="unfinished">程序产生的随机数字：</translation>
    </message>
    <message>
        <source>validation text imputed by user:</source>
        <translation type="unfinished">用户输入验证数字：</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="unfinished">警示</translation>
    </message>
    <message>
        <source>ensure the imput to identify random_text!</source>
        <translation type="unfinished">确定输入数字与随机产生数字一致！</translation>
    </message>
</context>
<context>
    <name>ColumShowConf</name>
    <message>
        <source>check colum which is need</source>
        <translation type="unfinished">选择需要的列</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
</context>
<context>
    <name>ImportConf</name>
    <message>
        <source>importConf</source>
        <translation type="unfinished">导入配置</translation>
    </message>
    <message>
        <source>user can set the div string with combobox and set a row which is more than 0 and a start row of the data will be imported.user can set the map for the column of the import data list and the dispaly list all so.warring, the column&apos;s index is start with 0.</source>
        <translation type="unfinished">你可以在下拉框中选择分隔符，可以设置大于0的数来指定在你的导入数据的起始行。
你可以设置你导入数据的列与显示列表的列的映射关系，注意，列的索引时从0开始的。</translation>
    </message>
    <message>
        <source>filepath:</source>
        <translation type="unfinished">文件路径：</translation>
    </message>
    <message>
        <source>set</source>
        <translation type="unfinished">设置</translation>
    </message>
    <message>
        <source>xmlNode:</source>
        <translation type="unfinished">xml节点：</translation>
    </message>
    <message>
        <source>divCheck:</source>
        <translation type="unfinished">分隔选择：</translation>
    </message>
    <message>
        <source>startRow:</source>
        <translation type="unfinished">起始行：</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <source>xml nodes set</source>
        <translation type="unfinished">xml节点设置</translation>
    </message>
    <message>
        <source>read xml, the xml node isn&apos;t set, please set it.</source>
        <translation type="unfinished">读取xml信息，xml节点未设置，请重新设置它。</translation>
    </message>
    <message>
        <source>Open Dialog</source>
        <translation type="unfinished">打开对话框</translation>
    </message>
    <message>
        <source>file %1 is not exist, please check it.</source>
        <translation type="unfinished">文件%1不存在，请检查确认。</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Exit</source>
        <translation type="unfinished">退出</translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit the application</source>
        <translation type="unfinished">退出程序</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">关于</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>app info</source>
        <translation type="unfinished">程序信息</translation>
    </message>
    <message>
        <source>file</source>
        <translation type="unfinished">文件</translation>
    </message>
    <message>
        <source>help</source>
        <translation type="unfinished">帮助</translation>
    </message>
    <message>
        <source>about</source>
        <translation type="unfinished">关于</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation type="unfinished">已准备</translation>
    </message>
    <message>
        <source>appName:%1
Version:%2
Company:%3
Tele:%4
Fax:%5
Web Site:%6
Address:%7
PostCode:%8</source>
        <translation type="unfinished">程序名称:%1
版本信息:%2
公司名称:%3
联系电话:%4
公司传真:%5
公司网站:%6
公司地址:%7
邮政编码:%8</translation>
    </message>
    <message>
        <source>Comparison</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Comparison for tran</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>app manage interface</source>
        <translation type="unfinished">程序管理接口</translation>
    </message>
    <message>
        <source>Min(&amp;I)</source>
        <translation type="unfinished">隐藏(&amp;I)</translation>
    </message>
    <message>
        <source>Rec(&amp;R)</source>
        <translation type="unfinished">恢复(&amp;R)</translation>
    </message>
    <message>
        <source>Help(&amp;H)</source>
        <translation type="unfinished">帮助(&amp;H)</translation>
    </message>
    <message>
        <source>Quit(&amp;Q)</source>
        <translation type="unfinished">退出(&amp;Q)</translation>
    </message>
    <message>
        <source>SaveEnsure</source>
        <translation type="unfinished">存储确认</translation>
    </message>
    <message>
        <source>The application will be closed.</source>
        <translation type="unfinished">程序将要关闭。</translation>
    </message>
    <message>
        <source>Do you want to save the data info?</source>
        <translation type="unfinished">你是否需要保存数据信息？</translation>
    </message>
    <message>
        <source>demo_for_DL_SensorAcquisition</source>
        <translation type="unfinished">大连传感器采集数据演示</translation>
    </message>
</context>
<context>
    <name>MyFileDialog</name>
    <message>
        <source>File Path Set Dialog</source>
        <translation type="unfinished">文件路径设置对话框</translation>
    </message>
    <message>
        <source>MyLookIn</source>
        <translation type="unfinished">浏览</translation>
    </message>
    <message>
        <source>MyfileName</source>
        <translation type="unfinished">文件名称</translation>
    </message>
    <message>
        <source>MyFileType</source>
        <translation type="unfinished">文件类型</translation>
    </message>
    <message>
        <source>mySave</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <source>csv Files (*.csv)</source>
        <translation type="unfinished">csv 文件 (*.csv)</translation>
    </message>
    <message>
        <source>xml Files (*.xml)</source>
        <translation type="unfinished">xml 文件 (*.xml)</translation>
    </message>
    <message>
        <source>myOpen</source>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <source>csv Files (*.csv);;ini files (*.ini);;Text files (*.txt);;xml files (*.xml);;Any files (*)</source>
        <translation type="unfinished">csv 文件 (*.csv);;ini 文件 (*.ini);;Text 文件 (*.txt);;xml 文件 (*.xml);;所有文件 (*)</translation>
    </message>
    <message>
        <source>Any files (*)</source>
        <translation type="unfinished">所有文件 (*)</translation>
    </message>
    <message>
        <source>myCancel</source>
        <translation type="unfinished">取消</translation>
    </message>
</context>
<context>
    <name>MyTableView</name>
    <message>
        <source>removeRow</source>
        <translation type="unfinished">删除行</translation>
    </message>
    <message>
        <source>insertRow</source>
        <translation type="unfinished">插入行</translation>
    </message>
    <message>
        <source>importFile</source>
        <translation type="unfinished">导入数据</translation>
    </message>
    <message>
        <source>saveFile</source>
        <translation type="unfinished">保存数据</translation>
    </message>
    <message>
        <source>exception Error</source>
        <translation type="unfinished">异常信息</translation>
    </message>
    <message>
        <source>Save Dialog</source>
        <translation type="unfinished">存储对话框</translation>
    </message>
    <message>
        <source>Cannot write file %1:
%2.</source>
        <translation type="unfinished">无法写入文件 %1:
%2.</translation>
    </message>
</context>
<context>
    <name>PlotWidget</name>
    <message>
        <source>count</source>
        <translation type="unfinished">数量</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>error info: </source>
        <translation type="unfinished">错误信息：</translation>
    </message>
    <message>
        <source>EXCEL object loss</source>
        <translation type="unfinished">EXCEL 对象丢失</translation>
    </message>
    <message>
        <source>mid:%1,msg:%2, recv:%3,send successful</source>
        <translation type="unfinished">编号:%1,信息:%2, 回复:%3,发送成功</translation>
    </message>
    <message>
        <source>mid:%1,msg:%2, send fail,cause:%3</source>
        <translation type="unfinished">编号:%1,信息:%2, 发送失败，原因:%3</translation>
    </message>
    <message>
        <source>accompanying-data:</source>
        <translation type="unfinished">伴随数据：</translation>
    </message>
    <message>
        <source>overflow</source>
        <translation type="unfinished">溢出</translation>
    </message>
    <message>
        <source>block</source>
        <translation type="unfinished">块</translation>
    </message>
    <message>
        <source>replace</source>
        <translation type="unfinished">替代</translation>
    </message>
    <message>
        <source>non-present-value</source>
        <translation type="unfinished">非持久值</translation>
    </message>
    <message>
        <source>invalid-value</source>
        <translation type="unfinished">无效值</translation>
    </message>
    <message>
        <source>note-RecordF</source>
        <translation type="unfinished">是否开始记录功能</translation>
    </message>
    <message>
        <source>note-ListenPort</source>
        <translation type="unfinished">服务端口</translation>
    </message>
    <message>
        <source>note-dbDir</source>
        <translation type="unfinished">数据目录</translation>
    </message>
</context>
<context>
    <name>SAQueryConfView</name>
    <message>
        <source>file</source>
        <translation type="unfinished">文件</translation>
    </message>
    <message>
        <source>startTime:</source>
        <translation type="unfinished">起始时间:</translation>
    </message>
    <message>
        <source>endTime:</source>
        <translation type="unfinished">结束时间:</translation>
    </message>
    <message>
        <source>ReloadSA</source>
        <translation type="unfinished">重载数据</translation>
    </message>
    <message>
        <source>Draw</source>
        <translation type="unfinished">绘图</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Steps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sticks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SAView</name>
    <message>
        <source>devid</source>
        <translation type="unfinished">设备编号</translation>
    </message>
    <message>
        <source>idx</source>
        <translation type="unfinished">点编号</translation>
    </message>
    <message>
        <source>dtime</source>
        <translation type="unfinished">时间</translation>
    </message>
    <message>
        <source>value</source>
        <translation type="unfinished">值</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="unfinished">警示</translation>
    </message>
    <message>
        <source>start time more then end time, please reset it!</source>
        <translation type="unfinished">开始时间大于结束时间，请重设！</translation>
    </message>
</context>
</TS>
