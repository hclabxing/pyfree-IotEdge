//
// **********************************************************************

#ifndef PCS_INTERFACE_I_H
#define PCS_INTERFACE_I_H

#include "PCSInterface.h"

class PCSClient;

class ClientAchieveI : public PCS::ClientAchieve
{
public:
	ClientAchieveI(PCSClient* _client);
    ~ClientAchieveI();
    virtual void addDev(const ::PCS::Devs &devs, const ::Ice::Current&);
    virtual void addPInfo(::Ice::Long devID, const ::PCS::PInfos &pinfos, const ::Ice::Current&);
    virtual void PValueChange(::Ice::Long devID
                              ,::Ice::Long pID
                              , const ::PCS::DateTimeI& itime
                              , ::Ice::Float val
                              , const ::Ice::Current&);
private:
	PCSClient* client;
};

#endif
