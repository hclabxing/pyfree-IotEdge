/****************************************************************************
**
** Copyright (C) 2017
**
****************************************************************************/
import QtQuick 2.2
import QtQuick.Controls 1.4

FocusScope {
    id: wrapper

    property alias text: input.text
    property alias inputMethodHints: input.inputMethodHints
    property alias color:input.color
//    property alias cursorVisible: input.cursorVisible

    signal accepted

    Rectangle {
        anchors.fill: parent
        border.color: "#707070"
        color: "white"
        radius: 4  //半径

        TextInput {
            id: input
            focus: true
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter  //垂直对齐
            font.pixelSize: Qt.platform.os === "android"?72:36
            //color: "#707070"
            color: "black"
//            validator: RegExpValidator { regExp: /^\d+(\.\d{0,3})?$/ }
            onAccepted: wrapper.accepted()  //链接到信号
        }
    }
}
