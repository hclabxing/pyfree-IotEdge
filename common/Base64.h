#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef __BASE_64_H__
#define __BASE_64_H__
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : Base64.h
  *File Mark       : 
  *Summary         : 
  *base64编码相关函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

  *modify		   : windows(CRLF)格式指定
 ************************************************************************/
#include <string>

namespace pyfree
{
	/**
	 * 编码
	 * @param Data {const uchar*} 编码内容指针
	 * @param DataByte {int} 输入的数据长度,以字节为单位
	 * @return {string} 编码结果
	 */
	std::string Encode(const unsigned char* Data, int DataByte);
	/**
	 * 解码
	 * @param Data {const uchar*} 解码内容指针
	 * @param DataByte {int} 输入的数据长度,以字节为单位
	 * @param OutByte {int} 输出的数据长度,以字节为单位,请不要通过返回值计算
	 * @return {string} 解码结果
	 */
	std::string Decode(const char* Data, int DataByte, int& OutByte);
	/**
	 * 图片转base
	 * @param imgPath {string} 图片路径
	 * @param imgBase64 {string} 转换结果
	 * @return {bool} 是否成功
	 */
	bool ImageToBase64(std::string imgPath,std::string &imgBase64);
	//static bool Base64ToImage(std::string baseBuf,std::string imgPath);//fail save image format info
	//以下是百度SDK包的实现
	/**
	 * base64编码
	 * @param bytes_to_encode {const uchar*}
	 * @param in_len {uint}
	 * @return {string}
	 */
	std::string base64_encode(const char * bytes_to_encode, unsigned int in_len);
	/**
	 * base64解码
	 * @param encoded_string {string}
	 * @return {string}
	 */
	std::string base64_decode(std::string const & encoded_string);
};

#endif
