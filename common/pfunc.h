#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PFUNC_H_
#define _PFUNC_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : pfunc.h
  *File Mark       : 
  *Summary         : 通用函数集1
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "pfunc_time.h"
#include "pfunc_str.h"
#include "pfunc_ip.h"
#include "pfunc_code.h"
#include "pfunc_format.h"
#include "pfunc_print.h"

#endif
