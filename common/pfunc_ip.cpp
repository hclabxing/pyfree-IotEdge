#include "pfunc_ip.h"

#ifdef WIN32
#include <Winsock2.h>
#else
#include <netinet/in.h>
#include <arpa/inet.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "pfunc_print.h"

bool pyfree::ipCheck(std::string ip_str)
{
	if (INADDR_NONE != inet_addr(ip_str.c_str())) 
	{
		return true;
	}
	return false;
};

long pyfree::ipToInt(std::string ip_str)
{
	if (INADDR_NONE != inet_addr(ip_str.c_str())) 
	{
		return ntohl(inet_addr(ip_str.c_str()));
	}
	else {
		Print_WARN("ip format [%s] erro,please check the file format and code!"
			, ip_str.c_str());
		return 0;
	}
};
std::string pyfree::intToIp(long ip_int)
{
	char ip[64] = { 0 };
#ifdef WIN32
	strcpy_s(ip, inet_ntoa(*(in_addr*)&ip_int));
#else
	strcpy(ip, inet_ntoa(*(in_addr*)ip_int));
#endif
	return std::string(ip);
};