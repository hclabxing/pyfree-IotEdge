
#ifndef _TASK_H_
#define _TASK_H_

#ifdef WIN32
#include <time.h>
#include <process.h>
#include <direct.h>
#include <io.h>
// #include "windows.h"
#define ACCESS _access
#define MKDIR(a) _mkdir((a))
#define localtime_x(x,y) localtime_s(x,y)
#define BASETIME 116444736000000000L
#define mssleep(x) Sleep(x)
#define winapi unsigned int __stdcall
#define pthread HANDLE
#else//linux
#include <time.h>
#include <stdarg.h>
#include <sys/stat.h>
#define ACCESS access
#define MKDIR(a) mkdir((a),0755)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define sprintf_s snprintf
#define localtime_x(x,y) localtime_r(y,x)
#define BASETIME 116444736000000000ll
#define __int64 long long
#include <unistd.h>
#define mssleep(x) usleep(x * 1000)
#include <sys/vfs.h>
#include <mntent.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/types.h>
#define winapi void*
#define pthread pthread_t
#endif

#endif
