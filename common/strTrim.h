#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _STRTRIM_H_
#define _STRTRIM_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : strTrim.h
  *File Mark       : 
  *Summary         : 
  *字符串前后空格处置函数
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#include <string>
#include <vector>
#include <algorithm>
#include <functional>

namespace pyfree
{
/**
 * 删除字符串前面的空格
 * @param ss {string&} 传入字符串
 * @return {string& } 返回字符串
 */
inline std::string& lTrim(std::string &ss)
{
	//std::string::iterator   p=find_if(ss.begin(),ss.end(),std::not1(std::ptr_fun(isspace)));
    //ss.erase(ss.begin(),p);
	ss.erase(0, ss.find_first_not_of(" \t")); //
    return  ss;
}
/**
 * 删除字符串后面的空格
 * @param ss {string&} 传入字符串
 * @return {string& } 返回字符串
 */
inline  std::string& rTrim(std::string &ss)
{
	//std::string::reverse_iterator  p=find_if(ss.rbegin(),ss.rend(),std::not1(std::ptr_fun(isspace)));
    //ss.erase(p.base(),ss.end());
	ss.erase(ss.find_last_not_of(" \t") + 1); //
    return   ss;
}
/**
 * 删除字符串前面和后面的空格
 * @param ss {string&} 传入字符串
 * @return {string& } 返回字符串
 */
inline  std::string& trim(std::string &st)
{
    lTrim(rTrim(st));
    return   st;
}
/**
 * 擦除字符串后面的结束字符
 * @param ss {string&} 传入字符串
 * @return {string& } 返回字符串
 */
inline void trimEnd(std::string &dir)
{
    if(dir.empty()){
        return;
    }
	//some line is NULL
	if(dir.size()==1){
		return;
	}
    while(dir[dir.length()-1] == '\n'
		|| dir[dir.length()-1] == '\r'
		|| dir[dir.length()-1] == '\t')
    {
        dir = dir.substr(0,dir.size()-1);
    }
};

};

#endif //