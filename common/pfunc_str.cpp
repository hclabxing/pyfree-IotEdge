#include "pfunc_str.h"

bool pyfree::string_divide(std::vector<std::string> &_strlist, const std::string src, const std::string div)
{
	std::string _src = src;
	std::string::size_type _pos = _src.find(div);
	while (std::string::npos != _pos)
	{
		std::string _buf = "";
		_buf = _src.substr(0, _pos);
		_strlist.push_back(_buf);
		_src = _src.erase(0, _pos + div.size());
		_pos = _src.find(div.c_str());
	}
	if (!_src.empty()) 
	{
		_strlist.push_back(_src);
	}
	return true;
};

bool pyfree::string_join(const std::vector<std::string> _strlist, std::string &src, const std::string div)
{
	src = "";
	for (unsigned int i= 0; i<_strlist.size();i++)
	{
		src += _strlist.at(i);
		if ((_strlist.size() - 1) != i) {
			src += div;
		}
	}
	if (src.empty())
		return false;
	else
		return true;
};

bool pyfree::containStr(const std::string _str, const std::string _strsub)
 {
     std::string::size_type pos = _str.find(_strsub);//
     if(pos!=std::string::npos)
	 {
         return true;
     }
     return false;
 };

bool pyfree::containStr_start(const std::string _str, const std::string _strsub)
{
	std::string::size_type pos = _str.find(_strsub);//查找_strsub标识
	if(pos==0)
	{//存在并从起始位置查找到
		return true;
	}
	return false;
};