#include "strchange.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef __linux__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iconv.h>
#endif
#ifdef WIN32
#include <iostream>
#include <windows.h>
#endif
#include <vector>

#ifdef WIN32
//UTF-8 to Unicode
std::wstring pyfree::Utf82Unicode(const std::string& utf8string)
{
	int widesize = ::MultiByteToWideChar(CP_UTF8, 0, utf8string.c_str(), -1, NULL, 0);
	if (widesize == ERROR_NO_UNICODE_TRANSLATION)
	{
		throw std::exception("Invalid UTF-8 sequence.");
	}
	if (widesize == 0)
	{
		throw std::exception("Error in conversion.");
	}

	std::vector<wchar_t> resultstring(widesize);

	int convresult = ::MultiByteToWideChar(CP_UTF8, 0, utf8string.c_str(), -1, &resultstring[0], widesize);

	if (convresult != widesize)
	{
		throw std::exception("La falla!");
	}

	return std::wstring(&resultstring[0]);
};

//unicode to ascii

std::string pyfree::WideByte2Acsi(std::wstring& wstrcode)
{
	int asciisize = ::WideCharToMultiByte(CP_OEMCP, 0, wstrcode.c_str(), -1, NULL, 0, NULL, NULL);
	if (asciisize == ERROR_NO_UNICODE_TRANSLATION)
	{
		throw std::exception("Invalid UTF-8 sequence.");
	}
	if (asciisize == 0)
	{
		throw std::exception("Error in conversion.");
	}
	std::vector<char> resultstring(asciisize);
	int convresult =::WideCharToMultiByte(CP_OEMCP, 0, wstrcode.c_str(), -1, &resultstring[0], asciisize, NULL, NULL);

	if (convresult != asciisize)
	{
		throw std::exception("La falla!");
	}

	return std::string(&resultstring[0]);
};

///////////////////////////////////////////////////////////////////////

//ascii to Unicode

std::wstring pyfree::Acsi2WideByte(std::string& strascii)
{
	int widesize = MultiByteToWideChar (CP_ACP, 0, (char*)strascii.c_str(), -1, NULL, 0);
	if (widesize == ERROR_NO_UNICODE_TRANSLATION)
	{
		throw std::exception("Invalid UTF-8 sequence.");
	}
	if (widesize == 0)
	{
		throw std::exception("Error in conversion.");
	}
	std::vector<wchar_t> resultstring(widesize);
	int convresult = MultiByteToWideChar (CP_ACP, 0, (char*)strascii.c_str(), -1, &resultstring[0], widesize);

	if (convresult != widesize)
	{
		throw std::exception("La falla!");
	}

	return std::wstring(&resultstring[0]);
};

//Unicode to Utf8

std::string pyfree::Unicode2Utf8(const std::wstring& widestring)
{
	int utf8size = ::WideCharToMultiByte(CP_UTF8, 0, widestring.c_str(), -1, NULL, 0, NULL, NULL);
	if (utf8size == 0)
	{
		throw std::exception("Error in conversion.");
	}

	std::vector<char> resultstring(utf8size);

	int convresult = ::WideCharToMultiByte(CP_UTF8, 0, widestring.c_str(), -1, &resultstring[0], utf8size, NULL, NULL);

	if (convresult != utf8size)
	{
		throw std::exception("La falla!");
	}

	return std::string(&resultstring[0]);
};

std::wstring pyfree::stringToWstring(const std::string& str)
{
    LPCSTR pszSrc = str.c_str();
    int nLen = MultiByteToWideChar(CP_ACP, 0, pszSrc, -1, NULL, 0);
    if (nLen == 0) 
        return std::wstring(L"");
 
    wchar_t* pwszDst = new wchar_t[nLen];
    if (!pwszDst) 
        return std::wstring(L"");
 
    MultiByteToWideChar(CP_ACP, 0, pszSrc, -1, pwszDst, nLen);
    std::wstring wstr(pwszDst);
    delete[] pwszDst;
    pwszDst = NULL;
 
    return wstr;
}

std::string pyfree::wstringToString(const std::wstring& wstr)
{
	LPCWSTR pwszSrc = wstr.c_str();
	int nLen = WideCharToMultiByte(CP_ACP, 0, pwszSrc, -1, NULL, 0, NULL, NULL);
	if (nLen == 0) 
		return std::string("");
	
	char* pszDst = new char[nLen];
	if (!pszDst) 
		return std::string("");
	
	WideCharToMultiByte(CP_ACP, 0, pwszSrc, -1, pszDst, nLen, NULL, NULL);
	std::string str(pszDst);
	delete[] pszDst;
	pszDst = NULL;
	
	return str;
}

#endif

#ifdef __linux__
//
int pyfree::code_convert(char *from_charset,char *to_charset,char *inbuf,size_t inlen,char *outbuf,size_t outlen)
{
    iconv_t cd;
    //int rc;
    char **pin = &inbuf;
    char **pout = &outbuf;

    cd = iconv_open(to_charset,from_charset);
    if (cd==0) return -1;
    memset(outbuf,0,outlen);
    if (-1==static_cast<int>(iconv(cd,pin,&inlen,pout,&outlen)))
	return -1;
    iconv_close(cd);
    return 0;
}
//
int pyfree::u2g(char *inbuf,int inlen,char *outbuf,int outlen)
{
	char _fbuf[32]="utf-8";
	char _tbuf[32]="gb2312";
    return code_convert(_fbuf,_tbuf,inbuf,inlen,outbuf,outlen);
}
//
int pyfree::g2u(char *inbuf,size_t inlen,char *outbuf,size_t outlen)
{
	char _fbuf[32]="gb2312";
	char _tbuf[32]="utf-8";
    return code_convert(_fbuf,_tbuf,inbuf,inlen,outbuf,outlen);
}

int pyfree::u2a(char *inbuf,int inlen,char *outbuf,int outlen)
{
	char _fbuf[32]="utf-8";
	char _tbuf[32]="ascii";
    return code_convert(_fbuf,_tbuf,inbuf,inlen,outbuf,outlen);
}

int pyfree::a2u(char *inbuf,int inlen,char *outbuf,int outlen)
{
	char _fbuf[32]="ascii";
	char _tbuf[32]="utf-8";
    return code_convert(_fbuf,_tbuf,inbuf,inlen,outbuf,outlen);
}

int pyfree::u2k(char *inbuf,int inlen,char *outbuf,int outlen)
{
	char _fbuf[32]="utf-8";
	char _tbuf[32]="gbk";
    return code_convert(_fbuf,_tbuf,inbuf,inlen,outbuf,outlen);
}

int pyfree::k2u(char *inbuf,int inlen,char *outbuf,int outlen)
{
	char _fbuf[32]="gbk";
	char _tbuf[32]="utf-8";
    return code_convert(_fbuf,_tbuf,inbuf,inlen,outbuf,outlen);
}

#endif

//utf-8 to ascii

std::string pyfree::UTF_82ASCII(std::string& strUtf8Code)
{
	#ifdef WIN32
	std::string strRet("");
	//
	std::wstring wstr = Utf82Unicode(strUtf8Code);
	//
	strRet = WideByte2Acsi(wstr);
	return strRet;
	#endif
	#ifdef __linux__
	char lpszBuf[1024]={0};
	u2k(const_cast<char*>(strUtf8Code.c_str()),strUtf8Code.size(),lpszBuf, 1024);
	return std::string(lpszBuf);
	#endif
};

//ascii to Utf8

std::string pyfree::ASCII2UTF_8(std::string& strAsciiCode)
{
	#ifdef WIN32
	std::string strRet("");
	//
	std::wstring wstr = Acsi2WideByte(strAsciiCode);
	//
	strRet = Unicode2Utf8(wstr);
	return strRet;
	#endif
	#ifdef __linux__
	char lpszBuf[1024]={0};
	k2u(const_cast<char*>(strAsciiCode.c_str()),strAsciiCode.size(),lpszBuf, 1024);
	return std::string(lpszBuf);
	#endif
};

