#!/bin/sh
commit_ts=`git log -1 --format="%ct"`
commit_time=`date -d@$commit_ts +"%Y-%m-%d %H:%M:%S"`
current_time=`date +"%Y-%m-%d %H:%M:%S"`
git_appinfo=`git log -1 --format="%h"`
commit_title=`git log -1 --format="%s"`
app_ver=`echo "${commit_title:0:5}"`
sed s/WCDATE/"$commit_time"/g ../src/appinfo.h__ > ../src/appinfo.h
sed -i s/WCNOW/"$current_time"/g ../src/appinfo.h
sed -i s/WCREV/"$git_appinfo"/g ../src/appinfo.h
sed -i s/WCURL/"https:\/\/"/g ../src/appinfo.h
sed -i s/WCVER/"$app_ver"/g ../src/appinfo.h
