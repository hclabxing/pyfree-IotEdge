#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef _UDP_IO_H_
#define _UDP_IO_H_
/***********************************************************************
  *Copyright 2020-06-06, pyfree
  *
  *File Name       : udp_io.h
  *File Mark       :
  *Summary         : 
  *数据存储通信接口实现,采用udp方式通信,作为接收方,单向接收来自调度软件推送的业务数据
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

class RecordQueue;

class UdpIO : public acl::thread
{
private:
    /* data */
    bool running;		    //线程运行标记
    std::string local_addr; //
    std::string peer_addr; //
    RecordQueue *ptr_rq;
public:
    UdpIO(std::string addr="127.0.0.1",int port=60004);
    ~UdpIO();

    void* run();
private:
    int AddFrameDef(const unsigned char *pBuf, int len);
};

#endif
