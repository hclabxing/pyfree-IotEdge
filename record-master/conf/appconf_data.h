#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _APP_CONF_DATA_H_
#define _APP_CONF_DATA_H_
/***********************************************************************
  *Copyright 2020-05-04, pyfree
  *
  *File Name       : conf_read.h
  *File Mark       : 
  *Summary         : 
  *程序运行参数集,由单体类业务数据实例继承调用
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "conf_app.h"

class AppConfData
{
protected:
    /* data */
    //app运行参数配置集
	pyfree::AppConf appConf;
public:
	//
	inline std::string getGLogDir()
	{
		return appConf.saconf.gLogDir;
	};
	inline char getDiskSymbol()
	{
		return appConf.saconf.diskSymbol;
	};
	inline int getFreeSizeLimit()
	{
		return appConf.saconf.freeSizeLimit;
	};
	inline int getDayForLimit()
	{
		return appConf.saconf.dayForLimit;
	};
	//record
	inline bool getRecordFunc()
	{
		return appConf.saconf.recordFunc;
	};
	inline std::string getGDataDir()
	{
		return appConf.saconf.gDataDir;
	};
	inline int getGMonFalg()
	{
		return appConf.saconf.m_MonRecord_b;
	};
	//udp
	inline std::string getLocalIp()
	{
		return appConf.udpconf.local_ip;
	};
	inline int getLocalPort()
	{
		return appConf.udpconf.local_port;
	};
protected:
    AppConfData(/* args */);
    ~AppConfData();
};

#endif
