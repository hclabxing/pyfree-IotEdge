#include "conf_read.h"

#include <map>
#include <fstream>
#include <time.h>

#include "pfunc.h"
#include "Log.h"
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

void read_SysAssistConf(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::SysAssistConf &saconf)
{
	while (pchild){
		if (0==strcmp("DiskSymbol",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (1 == strlen(val_))
				saconf.diskSymbol = val_[0];
		}
		//
		if (0==strcmp("FreeSizeLimit",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.freeSizeLimit = atoi(val_);
			if (saconf.freeSizeLimit < 1000)
				saconf.freeSizeLimit = 1000;
		}
		//
		if (0==strcmp("DayForLimit",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.dayForLimit = atoi(val_);
		}
		if (0==strcmp("LogDir",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.gLogDir = std::string(val_);
		}
		//
		if (0==strcmp("RecordFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				saconf.recordFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("DataCurveDir" , pchild->tag_name())) 
		{		
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				saconf.gDataDir = std::string(val_);
		}
		if (0==strcmp("DataIsMonRecord",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.m_MonRecord_b = atoi(val_);
		}
		pchild = pnode->next_child();
	}
	Print_NOTICE("\nSysAssistConf:\nDiskSymbol:%c\nFreeSizeLimit:%d\nDayForLimit:%d\nLogDir:%s\n"
		"RecordFunc:%d\nDataCurveDir:%s\nDataIsMonRecord:%d\r\n"
		,saconf.diskSymbol
		,saconf.freeSizeLimit
		,saconf.dayForLimit
		,saconf.gLogDir.c_str()
		,saconf.recordFunc
		,saconf.gDataDir.c_str()
		,saconf.m_MonRecord_b);
}

void read_AreaInfo(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::AreaInfo &ainfo)
{
	while (pchild){
		if (0==strcmp("AreaID",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				ainfo.areaId = atoi(val_);
		}
		if (0==strcmp("AreaType",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				ainfo.areaType = atoi(val_);
		}
		if (0==strcmp("AreaName",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				ainfo.areaName = std::string(val_);
		}
		if (0==strcmp("AreaDesc",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				ainfo.areaDesc = std::string(val_);
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nAreaInfo:\nAreaID:%d\nAreaType:%d\nAreaName:%s\nAreaDesc:%s\r\n"
	// 	,ainfo.areaId
	// 	,ainfo.areaType
	// 	,ainfo.areaName.c_str()
	// 	,ainfo.areaDesc.c_str());
}

void read_UdpIoInfo(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::UdpIoConf &udpinfo)
{
	while (pchild){
		if (0==strcmp("LocalIp",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				udpinfo.local_ip = std::string(val_);
		}
		if (0==strcmp("LocalPort",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				udpinfo.local_port = atoi(val_);
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nUdpInfo:\nLocalIp:%s\nLocalPort:%d\r\n"
	// 	,udpinfo.local_ip.c_str()
	// 	,udpinfo.local_port);
}
//
void pyfree::readAppConf(AppConf &conf, std::string xml_ )
{
	try
	{
        acl::string buf;
		if (acl::ifstream::load(xml_.c_str(), &buf) == false)
		{
			Print_WARN("load %s error %s\r\n", xml_.c_str(), acl::last_serror());
			return;
		}

		acl::xml1 xml;
		xml.update(buf);

		acl::xml_node* node 	= &(xml.get_root());
		acl::xml_node* child 	= node->first_child();
        while (child)
		{
			if(0==strcmp(child->tag_name(), "appconf"))
			{
				node	= child;
				child 	= node->first_child();
				while (child)
				{
					//
					if (0==strcmp("SysAssistConf",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_SysAssistConf(pnode,pchild,conf.saconf);
					}
					if (0==strcmp("AreaInfo",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_AreaInfo(pnode,pchild,conf.ainfo);
					}
					if (0==strcmp("UdpIoInfo",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_UdpIoInfo(pnode,pchild,conf.udpconf);
					}
					child = node->next_child();
				}
				break;
			}
			child = node->next_child();
		}
		CLogger::createInstance()->Log(MsgInfo,"success for read %s!\n",xml_.c_str());
	}
	catch (...)
	{
		CLogger::createInstance()->Log(MsgError,
			"read xml file[%s] error: %s %s %d,please check the file format and code!\n"
			, xml_.c_str(), __FILE__, __FUNCTION__, __LINE__);
	}
};

void pyfree::writeAppConf(const AppConf conf, std::string xml_)
{
	try {
		
	}
	catch (...)
	{
		CLogger::createInstance()->Log(MsgError
			, "write xml file[%s] error[%s]: %s %s %d"
			", please check the code!"
			, xml_.c_str()
			,__FILE__, __FUNCTION__, __LINE__);
	}
};
