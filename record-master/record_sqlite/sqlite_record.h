#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef _SQLITE_RECORD_H_
#define _SQLITE_RECORD_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : sqlite_record.h
  *File Mark       :
  *Summary         : 
  *数据记录线程
  *数据存储在指定目录下,每个设备建立一个存储目录,记录信息可以按天或按月存储在一个sqlite文件中
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include <string>

class SqliteIO;
class RecordQueue;

class SqliteRecord : public acl::thread
{
public:
	SqliteRecord();
	~SqliteRecord();

    void* run();
private:
	/**
	 * 依据设备编号在存储目录构建设备编号命名的文件目录
	 * @param devID {long } 设备编号
	 * @return {void}
	 */
	void mkDirForDev(long devID);
	/**
	 * 打开sqlite文件
	 * @param _path {string } sqlite文件路径
	 * @return {bool} 是否成功
	 */
	bool openDB(std::string _path);
	/**
	 * 依据设备编号在存储目录的存储文件名(无扩展名)
	 * @param devID {long } 设备编号
	 * @return {string} 文件名(带路径)
	 */
	std::string getCurDT_Str(long devID);
private:
	bool running;		//线程运行标记
	std::string dbDir;	//存储总目录
	int m_MonRecord_b;	//存储选择标记(1,按日一个文件 2,按月一个文件)
	SqliteIO *sio_ptr;	//数据库接口
	int recordF;		//记录标记,辅助用于定期批量记录
	RecordQueue *ptr_rq;//将要记录信息的缓存队列
};
#endif //
